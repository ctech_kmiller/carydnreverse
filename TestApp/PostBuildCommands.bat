﻿:@echo on
:: ====================================================================
:: File = PostBuildCommands.bat for TestApp.dll
::  7/Jan/2016
::
::  %1  = ConfigurationName = debug or release
::  %2 = TargetPath
:: ====================================================================

Setlocal
for /f %%i in ("%0") do set curpath=%%~dpi 
Echo Location %curpath%
For /f "tokens=1,2 delims=: " %%G in ('echo %curpath%') Do (set drive=%%G& set _location=%%H) 
Echo Drive %drive%

set configurationName=%1
set targetPath=%2
:: Remove the surrounding quote characters.
set targetPath=%targetPath:"=%
Echo TargetPath = %targetPath%

:: This is the target folder for the TestApp executable.
:: Set this to the folder in which the Cary UV software has been installed.
set winuvDir=%drive%:\Program Files\Agilent\Cary WinUV\
echo Cary WinUV Directory = %winuvDir%

if /I %configurationName% == Debug goto doDebug
if /I %configurationName% == Release goto doRelease
goto USAGE

:doDebug
echo Executing TestApp Post Build Event Debug commands
xcopy /Y "%targetPath%" "%winuvDir%"
goto end

:doRelease
echo Executing TestApp Post Build Event Release commands
xcopy /Y "%targetPath%" "%winuvDir%"
goto end

:USAGE
echo Usage:
echo.
echo PostBuildCommands ConfigurationName TargetPath
echo.
pause

:end