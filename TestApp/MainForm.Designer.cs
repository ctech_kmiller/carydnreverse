﻿namespace TestApp
{
	partial class MainForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.btnScan = new System.Windows.Forms.Button();
			this.m_propertyGrid = new System.Windows.Forms.PropertyGrid();
			this.btnRead = new System.Windows.Forms.Button();
			this.btnSetup = new System.Windows.Forms.Button();
			this.btnDefaultSetup = new System.Windows.Forms.Button();
			this.btnStopScan = new System.Windows.Forms.Button();
			this.txtReadings = new System.Windows.Forms.TextBox();
			this.menuStrip1 = new System.Windows.Forms.MenuStrip();
			this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.splitContainer1 = new System.Windows.Forms.SplitContainer();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.txtReadWL = new System.Windows.Forms.TextBox();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.txtStopWL = new System.Windows.Forms.TextBox();
			this.txtStartWL = new System.Windows.Forms.TextBox();
			this.label5 = new System.Windows.Forms.Label();
			this.btnModel = new System.Windows.Forms.Button();
			this.label4 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.btnGetStatus = new System.Windows.Forms.Button();
			this.btnTestSetup = new System.Windows.Forms.Button();
			this.txtAbs = new System.Windows.Forms.TextBox();
			this.listBoxMessages = new System.Windows.Forms.ListBox();
			this.txtRearBeam = new System.Windows.Forms.TextBox();
			this.txtFrontBeam = new System.Windows.Forms.TextBox();
			this.txtWavelength = new System.Windows.Forms.TextBox();
			this.txtResponse = new System.Windows.Forms.TextBox();
			this.statusStrip1 = new System.Windows.Forms.StatusStrip();
			this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
			this.menuStrip1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
			this.splitContainer1.Panel1.SuspendLayout();
			this.splitContainer1.Panel2.SuspendLayout();
			this.splitContainer1.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.groupBox1.SuspendLayout();
			this.statusStrip1.SuspendLayout();
			this.SuspendLayout();
			// 
			// btnScan
			// 
			this.btnScan.Location = new System.Drawing.Point(6, 19);
			this.btnScan.Name = "btnScan";
			this.btnScan.Size = new System.Drawing.Size(75, 23);
			this.btnScan.TabIndex = 0;
			this.btnScan.Text = "Start";
			this.btnScan.UseVisualStyleBackColor = true;
			this.btnScan.Click += new System.EventHandler(this.btnStartScan_Click);
			// 
			// m_propertyGrid
			// 
			this.m_propertyGrid.Dock = System.Windows.Forms.DockStyle.Fill;
			this.m_propertyGrid.Location = new System.Drawing.Point(0, 0);
			this.m_propertyGrid.Name = "m_propertyGrid";
			this.m_propertyGrid.Size = new System.Drawing.Size(370, 538);
			this.m_propertyGrid.TabIndex = 1;
			// 
			// btnRead
			// 
			this.btnRead.Location = new System.Drawing.Point(6, 25);
			this.btnRead.Name = "btnRead";
			this.btnRead.Size = new System.Drawing.Size(75, 23);
			this.btnRead.TabIndex = 4;
			this.btnRead.Text = "Read";
			this.btnRead.UseVisualStyleBackColor = true;
			this.btnRead.Click += new System.EventHandler(this.btnRead_Click);
			// 
			// btnSetup
			// 
			this.btnSetup.Location = new System.Drawing.Point(32, 65);
			this.btnSetup.Name = "btnSetup";
			this.btnSetup.Size = new System.Drawing.Size(75, 23);
			this.btnSetup.TabIndex = 5;
			this.btnSetup.Text = "Setup";
			this.btnSetup.UseVisualStyleBackColor = true;
			this.btnSetup.Click += new System.EventHandler(this.btnSetup_Click);
			// 
			// btnDefaultSetup
			// 
			this.btnDefaultSetup.Location = new System.Drawing.Point(32, 30);
			this.btnDefaultSetup.Name = "btnDefaultSetup";
			this.btnDefaultSetup.Size = new System.Drawing.Size(96, 23);
			this.btnDefaultSetup.TabIndex = 7;
			this.btnDefaultSetup.Text = "Default Setup";
			this.btnDefaultSetup.UseVisualStyleBackColor = true;
			this.btnDefaultSetup.Click += new System.EventHandler(this.btnDefaultSetup_Click);
			// 
			// btnStopScan
			// 
			this.btnStopScan.Location = new System.Drawing.Point(6, 54);
			this.btnStopScan.Name = "btnStopScan";
			this.btnStopScan.Size = new System.Drawing.Size(75, 23);
			this.btnStopScan.TabIndex = 8;
			this.btnStopScan.Text = "Stop";
			this.btnStopScan.UseVisualStyleBackColor = true;
			this.btnStopScan.Click += new System.EventHandler(this.btnStopScan_Click);
			// 
			// txtReadings
			// 
			this.txtReadings.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtReadings.Location = new System.Drawing.Point(98, 263);
			this.txtReadings.Name = "txtReadings";
			this.txtReadings.Size = new System.Drawing.Size(344, 26);
			this.txtReadings.TabIndex = 9;
			this.txtReadings.Visible = false;
			// 
			// menuStrip1
			// 
			this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
			this.menuStrip1.Location = new System.Drawing.Point(0, 0);
			this.menuStrip1.Name = "menuStrip1";
			this.menuStrip1.Size = new System.Drawing.Size(848, 24);
			this.menuStrip1.TabIndex = 10;
			this.menuStrip1.Text = "menuStrip1";
			// 
			// fileToolStripMenuItem
			// 
			this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
			this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
			this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
			this.fileToolStripMenuItem.Text = "File";
			// 
			// exitToolStripMenuItem
			// 
			this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
			this.exitToolStripMenuItem.Size = new System.Drawing.Size(92, 22);
			this.exitToolStripMenuItem.Text = "Exit";
			this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
			// 
			// splitContainer1
			// 
			this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.splitContainer1.Location = new System.Drawing.Point(0, 24);
			this.splitContainer1.Name = "splitContainer1";
			// 
			// splitContainer1.Panel1
			// 
			this.splitContainer1.Panel1.Controls.Add(this.groupBox2);
			this.splitContainer1.Panel1.Controls.Add(this.groupBox1);
			this.splitContainer1.Panel1.Controls.Add(this.label5);
			this.splitContainer1.Panel1.Controls.Add(this.btnModel);
			this.splitContainer1.Panel1.Controls.Add(this.label4);
			this.splitContainer1.Panel1.Controls.Add(this.label3);
			this.splitContainer1.Panel1.Controls.Add(this.label2);
			this.splitContainer1.Panel1.Controls.Add(this.label1);
			this.splitContainer1.Panel1.Controls.Add(this.btnGetStatus);
			this.splitContainer1.Panel1.Controls.Add(this.btnTestSetup);
			this.splitContainer1.Panel1.Controls.Add(this.txtAbs);
			this.splitContainer1.Panel1.Controls.Add(this.listBoxMessages);
			this.splitContainer1.Panel1.Controls.Add(this.txtRearBeam);
			this.splitContainer1.Panel1.Controls.Add(this.txtFrontBeam);
			this.splitContainer1.Panel1.Controls.Add(this.txtWavelength);
			this.splitContainer1.Panel1.Controls.Add(this.txtResponse);
			this.splitContainer1.Panel1.Controls.Add(this.txtReadings);
			this.splitContainer1.Panel1.Controls.Add(this.btnDefaultSetup);
			this.splitContainer1.Panel1.Controls.Add(this.btnSetup);
			// 
			// splitContainer1.Panel2
			// 
			this.splitContainer1.Panel2.Controls.Add(this.m_propertyGrid);
			this.splitContainer1.Size = new System.Drawing.Size(848, 538);
			this.splitContainer1.SplitterDistance = 474;
			this.splitContainer1.TabIndex = 11;
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.txtReadWL);
			this.groupBox2.Controls.Add(this.btnRead);
			this.groupBox2.Location = new System.Drawing.Point(259, 118);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(200, 70);
			this.groupBox2.TabIndex = 25;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Single Read";
			// 
			// txtReadWL
			// 
			this.txtReadWL.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtReadWL.Location = new System.Drawing.Point(103, 22);
			this.txtReadWL.Name = "txtReadWL";
			this.txtReadWL.Size = new System.Drawing.Size(71, 26);
			this.txtReadWL.TabIndex = 14;
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.txtStopWL);
			this.groupBox1.Controls.Add(this.txtStartWL);
			this.groupBox1.Controls.Add(this.btnScan);
			this.groupBox1.Controls.Add(this.btnStopScan);
			this.groupBox1.Location = new System.Drawing.Point(259, 11);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(200, 100);
			this.groupBox1.TabIndex = 24;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Scan";
			// 
			// txtStopWL
			// 
			this.txtStopWL.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtStopWL.Location = new System.Drawing.Point(103, 54);
			this.txtStopWL.Name = "txtStopWL";
			this.txtStopWL.Size = new System.Drawing.Size(71, 26);
			this.txtStopWL.TabIndex = 13;
			// 
			// txtStartWL
			// 
			this.txtStartWL.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtStartWL.Location = new System.Drawing.Point(103, 19);
			this.txtStartWL.Name = "txtStartWL";
			this.txtStartWL.Size = new System.Drawing.Size(71, 26);
			this.txtStartWL.TabIndex = 12;
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(34, 239);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(55, 13);
			this.label5.TabIndex = 23;
			this.label5.Text = "Response";
			// 
			// btnModel
			// 
			this.btnModel.Location = new System.Drawing.Point(134, 65);
			this.btnModel.Name = "btnModel";
			this.btnModel.Size = new System.Drawing.Size(75, 23);
			this.btnModel.TabIndex = 22;
			this.btnModel.Text = "Model";
			this.btnModel.UseVisualStyleBackColor = true;
			this.btnModel.Visible = false;
			this.btnModel.Click += new System.EventHandler(this.btnModel_Click);
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(34, 207);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(25, 13);
			this.label4.TabIndex = 21;
			this.label4.Text = "Abs";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(34, 175);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(60, 13);
			this.label3.TabIndex = 20;
			this.label3.Text = "Rear Beam";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(31, 143);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(61, 13);
			this.label2.TabIndex = 19;
			this.label2.Text = "Front Beam";
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(29, 111);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(65, 13);
			this.label1.TabIndex = 18;
			this.label1.Text = "Wavelength";
			// 
			// btnGetStatus
			// 
			this.btnGetStatus.Location = new System.Drawing.Point(134, 30);
			this.btnGetStatus.Name = "btnGetStatus";
			this.btnGetStatus.Size = new System.Drawing.Size(75, 23);
			this.btnGetStatus.TabIndex = 17;
			this.btnGetStatus.Text = "Status";
			this.btnGetStatus.UseVisualStyleBackColor = true;
			this.btnGetStatus.Visible = false;
			this.btnGetStatus.Click += new System.EventHandler(this.btnGetStatus_Click);
			// 
			// btnTestSetup
			// 
			this.btnTestSetup.Location = new System.Drawing.Point(265, 199);
			this.btnTestSetup.Name = "btnTestSetup";
			this.btnTestSetup.Size = new System.Drawing.Size(75, 23);
			this.btnTestSetup.TabIndex = 16;
			this.btnTestSetup.Text = "Test Setup";
			this.btnTestSetup.UseVisualStyleBackColor = true;
			this.btnTestSetup.Visible = false;
			this.btnTestSetup.Click += new System.EventHandler(this.btnTestSetup_Click);
			// 
			// txtAbs
			// 
			this.txtAbs.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtAbs.Location = new System.Drawing.Point(98, 199);
			this.txtAbs.Name = "txtAbs";
			this.txtAbs.ReadOnly = true;
			this.txtAbs.Size = new System.Drawing.Size(121, 26);
			this.txtAbs.TabIndex = 15;
			// 
			// listBoxMessages
			// 
			this.listBoxMessages.BackColor = System.Drawing.SystemColors.Window;
			this.listBoxMessages.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.listBoxMessages.FormattingEnabled = true;
			this.listBoxMessages.ItemHeight = 16;
			this.listBoxMessages.Location = new System.Drawing.Point(0, 310);
			this.listBoxMessages.Name = "listBoxMessages";
			this.listBoxMessages.Size = new System.Drawing.Size(474, 228);
			this.listBoxMessages.TabIndex = 14;
			// 
			// txtRearBeam
			// 
			this.txtRearBeam.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtRearBeam.Location = new System.Drawing.Point(98, 167);
			this.txtRearBeam.Name = "txtRearBeam";
			this.txtRearBeam.ReadOnly = true;
			this.txtRearBeam.Size = new System.Drawing.Size(121, 26);
			this.txtRearBeam.TabIndex = 13;
			// 
			// txtFrontBeam
			// 
			this.txtFrontBeam.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtFrontBeam.Location = new System.Drawing.Point(98, 135);
			this.txtFrontBeam.Name = "txtFrontBeam";
			this.txtFrontBeam.ReadOnly = true;
			this.txtFrontBeam.Size = new System.Drawing.Size(121, 26);
			this.txtFrontBeam.TabIndex = 12;
			// 
			// txtWavelength
			// 
			this.txtWavelength.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtWavelength.Location = new System.Drawing.Point(98, 103);
			this.txtWavelength.Name = "txtWavelength";
			this.txtWavelength.ReadOnly = true;
			this.txtWavelength.Size = new System.Drawing.Size(121, 26);
			this.txtWavelength.TabIndex = 11;
			// 
			// txtResponse
			// 
			this.txtResponse.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtResponse.Location = new System.Drawing.Point(98, 231);
			this.txtResponse.Name = "txtResponse";
			this.txtResponse.ReadOnly = true;
			this.txtResponse.Size = new System.Drawing.Size(344, 26);
			this.txtResponse.TabIndex = 10;
			// 
			// statusStrip1
			// 
			this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
			this.statusStrip1.Location = new System.Drawing.Point(0, 540);
			this.statusStrip1.Name = "statusStrip1";
			this.statusStrip1.Size = new System.Drawing.Size(848, 22);
			this.statusStrip1.TabIndex = 12;
			this.statusStrip1.Text = "statusStrip1";
			// 
			// toolStripStatusLabel1
			// 
			this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
			this.toolStripStatusLabel1.Size = new System.Drawing.Size(118, 17);
			this.toolStripStatusLabel1.Text = "toolStripStatusLabel1";
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(848, 562);
			this.Controls.Add(this.statusStrip1);
			this.Controls.Add(this.splitContainer1);
			this.Controls.Add(this.menuStrip1);
			this.MainMenuStrip = this.menuStrip1;
			this.Name = "MainForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "CaryDN Test App";
			this.menuStrip1.ResumeLayout(false);
			this.menuStrip1.PerformLayout();
			this.splitContainer1.Panel1.ResumeLayout(false);
			this.splitContainer1.Panel1.PerformLayout();
			this.splitContainer1.Panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
			this.splitContainer1.ResumeLayout(false);
			this.groupBox2.ResumeLayout(false);
			this.groupBox2.PerformLayout();
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.statusStrip1.ResumeLayout(false);
			this.statusStrip1.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button btnScan;
		private System.Windows.Forms.PropertyGrid m_propertyGrid;
		private System.Windows.Forms.Button btnRead;
		private System.Windows.Forms.Button btnSetup;
		private System.Windows.Forms.Button btnDefaultSetup;
		private System.Windows.Forms.Button btnStopScan;
		private System.Windows.Forms.TextBox txtReadings;
		private System.Windows.Forms.MenuStrip menuStrip1;
		private System.Windows.Forms.SplitContainer splitContainer1;
		private System.Windows.Forms.StatusStrip statusStrip1;
		private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
		private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
		private System.Windows.Forms.TextBox txtResponse;
		private System.Windows.Forms.TextBox txtWavelength;
		private System.Windows.Forms.TextBox txtFrontBeam;
		private System.Windows.Forms.TextBox txtRearBeam;
		private System.Windows.Forms.ListBox listBoxMessages;
		private System.Windows.Forms.TextBox txtAbs;
		private System.Windows.Forms.Button btnTestSetup;
		private System.Windows.Forms.Button btnGetStatus;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button btnModel;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.TextBox txtStopWL;
		private System.Windows.Forms.TextBox txtStartWL;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.TextBox txtReadWL;
	}
}

