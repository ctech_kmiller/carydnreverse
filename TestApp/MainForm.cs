﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Threading;
using System.Text;
using System.Windows.Forms;

using CaryDN;
using CaryDN.SCary32;

// Applications will only run in the folder [drive]\Program files (x86)\Agilent\Cary WinUV\
// Attempts to run apps else where will only result in tears.
namespace TestApp
{
	public partial class MainForm : Form
	{
		MainFormController m_controller;
		delegate void ShowStatusDelegate(CaryDN.StatusEventArgs e);
		delegate void ShowDataDelegate(CaryDN.DataEventArgs e);
		delegate void ShowValueInDelegate(CaryDN.ValueInEventArgs e);

		public MainForm()
		{
			InitializeComponent();
			this.Load += new EventHandler(MainForm_Load);
		}

		void MainForm_Load(object sender, EventArgs e)
		{
			Thread.CurrentThread.Name = "Main";
			toolStripStatusLabel1.Text = "";

			m_controller = new MainFormController(this);

			this.Text = m_controller.Title;
			if (m_controller.Instrument == null)
				return;

			m_propertyGrid.SelectedObject = m_controller.Instrument;

			m_controller.Instrument.Error += new CaryDN.ErrorEventHandler(Instrument_CaryError);
			m_controller.Instrument.StatusChanged += new CaryDN.StatusEventHandler(Instrument_CaryStatusChanged);
			m_controller.Instrument.DataReceived += new CaryDN.DataEventHandler(Instrument_OnCaryData);
			m_controller.Instrument.ValueInReceived += new CaryDN.ValueInEventHandler(Instrument_ValueInReceived);
			m_controller.Instrument.ParamWL += new CaryDN.CaryMessageEventHandler(Instrument_ParamWL);
			m_controller.Instrument.Connecting += new EventHandler(Instrument_Connecting);
			m_controller.Instrument.Connected += new EventHandler(Instrument_Connected);
			m_controller.Instrument.Disconnected += new EventHandler(Instrument_Disconnected);

			FormClosing += new FormClosingEventHandler(MainForm_FormClosing);
			Resize += new EventHandler(MainForm_Resize);
			m_controller.Connect();
#if DEBUG
	//		AccyTests();
#endif
		}

		void MainForm_Resize(object sender, EventArgs e)
		{
			listBoxMessages.Top = txtResponse.Top + 100;
		}

		void MainForm_FormClosing(object sender, FormClosingEventArgs e)
		{
			if (m_controller != null)
			{
				m_controller.Disconnect();
				m_controller.Dispose();
				m_controller = null;
			}
		}

		void ShowInListBox(string msg)
		{
			Thread th = Thread.CurrentThread;
			if (InvokeRequired)
				return;

			if (listBoxMessages.Items.Count > 100)
				listBoxMessages.Items.Clear();

			int index = listBoxMessages.Items.Add(msg);
			listBoxMessages.SelectedIndex = index;
		}

		void Instrument_Connected(object sender, EventArgs e)
		{
			toolStripStatusLabel1.Text = "Connected";
			GetScanData();
		}

		void Instrument_Connecting(object sender, EventArgs e)
		{
			toolStripStatusLabel1.Text = "Connecting";
		}

		void Instrument_Disconnected(object sender, EventArgs e)
		{
			toolStripStatusLabel1.Text = "Disconnected";
		}

		void DoShowData(DataEventArgs e)
		{
			DoShowData(e.Wavelength, e.FrontBeam, e.RearBeam);
			if (e.Wavelength < 100.0)
				return;

			StringBuilder sb = new StringBuilder();
			sb.AppendFormat("{0:####.#}  ", e.Wavelength);
			sb.AppendFormat("Front = {0}  ", e.FrontBeam);
			sb.AppendFormat("Rear = {0}", e.RearBeam);
			ShowInListBox(sb.ToString());
		}

		void DoShowData(float wavelength, float frontBeam, float rearBeam)
		{
			try
			{
				if (wavelength > 100.0)
					this.txtWavelength.Text = string.Format("{0:####.#}", wavelength);

				this.txtFrontBeam.Text = string.Format("{0}", frontBeam);
				this.txtRearBeam.Text = string.Format("{0}", rearBeam);

				double abs = 9.999;

				if (frontBeam > 0)
					abs = -Math.Log10(frontBeam);

				this.txtAbs.Text = string.Format("{0:0.####}", abs);
			}
			catch (Exception ex)
			{
				string msg = ex.Message;
			}

			Application.DoEvents();
		}

		void Instrument_OnCaryData(object sender, DataEventArgs e)
		{
			if (!this.InvokeRequired)
			{
				DoShowData(e);
				return;
			}

			object[] args = new object[1];
			args[0] = e;
			IAsyncResult result = this.BeginInvoke(new ShowDataDelegate(DoShowData), args);
		}

		void Instrument_ValueInReceived(object sender, ValueInEventArgs e)
		{
			if (!this.InvokeRequired)
			{
				DoShowValueIn(e);
				return;
			}

			object[] args = new object[1];
			args[0] = e;
			IAsyncResult result = this.BeginInvoke(new ShowValueInDelegate(DoShowValueIn), args);
		}

		void DoShowValueIn(CaryDN.ValueInEventArgs e)
		{
			if (e.ParameterID == InstrumentParameterID.RESP_WAVELENGTH)
			{
				txtResponse.Text = string.Format("Wavelength = {0:####.#}", e.FParam);
				return;
			}

			txtResponse.Text = string.Format("{0} = {1}", e.ParameterID, e.Value);
		}

		void Instrument_ParamWL(object sender, CaryMessageEventArgs e)
		{
			txtReadings.Text = string.Format("Param_WL {0}", e.LParam);
		}

		void Instrument_CaryError(object sender, CaryDN.ErrorEventArgs e)
		{
			string msg = e.GetErrorMessage();
			MessageBox.Show (msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
		}

		void Instrument_CaryStatusChanged(object sender, CaryDN.StatusEventArgs e)
		{
			if (!this.InvokeRequired)
			{
				DoShowStatus(e);
				return;
			}

			object[] args = new object[1];
			args[0] = e;
			IAsyncResult result = this.BeginInvoke(new ShowStatusDelegate(DoShowStatus), args);
		}

		void DoShowStatus(CaryDN.StatusEventArgs e)
		{
			if (e.MessageID == UserMessage.UM_CARY_VALUE_IN)
			{
				Debug.Assert(true);
			}

			string msg = CaryDN.InstrumentStatusCode.ToString(e.Value);
	//		txtStatus.Text = string.Format("{0} {1}", msg, DateTime.Now.Second);
	//		toolStripStatusLabel1.Text = string.Format("{0} {1}", msg, DateTime.Now.Second);
			toolStripStatusLabel1.Text = msg;
		}

		private void btnStartScan_Click(object sender, EventArgs e)
		{
			try
			{
				listBoxMessages.Items.Clear();
				m_controller.Instrument.ScanMode = ScanMode.Wavelength;
				m_controller.Instrument.ScanStart = Convert.ToSingle(txtStartWL.Text);
				m_controller.Instrument.ScanStop = Convert.ToSingle(txtStopWL.Text);
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString());
			}

		    while (true)
		    {
                Debug.WriteLine("Starting Scan...");
		        m_controller.StartScan();
            }
			
		}

		private void btnStopScan_Click(object sender, EventArgs e)
		{
			m_controller.StopScan();
		}

		private void btnRead_Click(object sender, EventArgs e)
		{
			try
			{
				float wavelength = Convert.ToSingle(txtReadWL.Text);
				m_controller.Instrument.Wavelength = wavelength;

                // Initiate the read.
                // The data is returned by a DataReceived event having DataEventArgs
                // containing the front and rear beam data.
                while(true)
                { float result = m_controller.Instrument.Read(wavelength);}

			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString());
			}
		}

		private void btnSetup_Click(object sender, EventArgs e)
		{
			m_controller.Setup();
		}

		private void btnDefaultSetup_Click(object sender, EventArgs e)
		{
			m_controller.Instrument.GetDefaultSetup();
			m_propertyGrid.Refresh();
			GetScanData();
		}

		private void GetScanData()
		{
			try
			{
				txtStartWL.Text = m_controller.Instrument.ScanStart.ToString();
				txtStopWL.Text = m_controller.Instrument.ScanStop.ToString();
				txtReadWL.Text = m_controller.Instrument.Wavelength.ToString();
			}
			catch
			{
			}
		}

		private void exitToolStripMenuItem_Click(object sender, EventArgs e)
		{
			m_controller.Disconnect();
			this.Close();
		}

		private void btnTestSetup_Click(object sender, EventArgs e)
		{
		//	m_controller.SetTestDefaults();
			m_propertyGrid.Refresh();
		}

		private void btnGetStatus_Click(object sender, EventArgs e)
		{
			m_controller.Instrument.GetStatus();
		}

		private void btnModel_Click(object sender, EventArgs e)
		{
			m_controller.Instrument.GetModel();
		}

		private void registerToolStripMenuItem_Click(object sender, EventArgs e)
		{
		}

		private void unregisterToolStripMenuItem_Click(object sender, EventArgs e)
		{
		}

		private void AccyTests()
		{
			int device = (int)DeviceAddress.Accessory;

			Accessory accy = m_controller.Instrument.Accessory;
			accy.GotoCell(5);
			accy.Measure(device);
			accy.Monitor(1, 2, 3, 4, 10);
			accy.Ramp(1, 1);
			accy.Stop();
		}
	}
}
