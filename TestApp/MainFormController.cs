﻿using System;
using System.Collections;
using System.Text;
using System.Windows.Forms;

using CaryDN;
// ReSharper disable ArrangeTypeMemberModifiers

namespace TestApp
{
	/// <summary>
	/// Using the MVC pattern.
	/// </summary>
	public class MainFormController : IDisposable
	{
		Form m_parent;
		InstrumentEx m_inst;

		public MainFormController(Form parent)
		{
			m_parent = parent;
			IntPtr handle = parent.Handle;

			try
			{
				m_inst = new InstrumentEx();

				// Assign the product key here.
				m_inst.Registration.Key = "B7405-43748-8CC19-CC176-67F60";
				m_inst.OnlineChanged += caryOnlineChanged;
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
		}

		public string Title
		{
			get
			{
				string result = "CaryDN TestApp";
				if (m_inst == null)
					return result;

			//	if (m_inst.Registration != null)
			//	{
			//		KRSRegDN.enumStatus status = m_inst.Registration.Status;
			//		result = string.Format("{0} ({1})", result, m_inst.Registration.GetMessage(status));
			//	}

				return result;
			}
		}

		public void Dispose()
		{
			if (m_inst != null)
			{
				m_inst.Dispose();
				m_inst = null;
			}

			m_parent = null;
		}

		void caryOnlineChanged(object sender, BoolValueEventArgs e)
		{
			if (e.Value)
			{
				m_inst.Stop();
				m_inst.SubSetup(SubSetupCode.SUB_PROM_MODEL);
			}
		}

		public InstrumentEx Instrument
		{
			get { return m_inst; }
		}

		public void Read()
		{
		}

		public void Setup()
		{
			m_inst.Setup();
		}

		public void StopScan()
		{
			m_inst.Stop();
		}

		public void StartScan()
		{
			if (m_inst.IsBusy)
				m_inst.Stop();

			if (m_inst.IsScanning)
			{
				m_inst.Stop();
				m_inst.WaitNotScanning();
			}

			m_inst.ScanMode = ScanMode.Wavelength;
			m_inst.Setup();
			m_inst.Start();
		}

		private void SetTestDefaults()
		{
			m_inst.GetDefaultSetup();

			m_inst.BeamMode = BeamMode.DoubleAutoSelect;
			m_inst.UvVisSlitWidth = 1;
			m_inst.SlitHeight = SlitHeight.Full;
			m_inst.UvVisInterval = -1;
			m_inst.UvVisWaveNumberInterval = 10;
			m_inst.UvVisAveraging = 80;

			m_inst.Wavelength = 500;
			m_inst.IsVisibleLampOn = true;
			m_inst.ScanStart = 600;
			m_inst.ScanStop = 400;
			m_inst.ScanMode = ScanMode.Wavelength;
		}

		public void Disconnect()
		{
			m_inst.Disconnect();
		}

		public void GetStatus()
		{
		}

		public void Connect()
		{
			try
			{
				if (m_inst.IsConnected)
					return;

				m_inst.Connect();
				SetTestDefaults();
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
		}
	}
}
