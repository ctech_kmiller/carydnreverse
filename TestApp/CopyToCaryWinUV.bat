cls
echo Copying %1
set dstDir="C:\Program Files\Agilent\Cary WinUV"
if not exist %dstDir% mkdir %dstDir%
xcopy /Y %1 %dstDir%\*.*
