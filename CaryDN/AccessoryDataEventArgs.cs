﻿// Decompiled with JetBrains decompiler
// Type: CaryDN.AccessoryDataEventArgs
// Assembly: CaryDN, Version=1.0.6.0, Culture=neutral, PublicKeyToken=null
// MVID: D1AE64FC-DE05-4F0D-A2F5-6382B4200B36
// Assembly location: C:\src\Decompile\Agilent\CaryDN\1.0.6.0\Decomp\CaryDN.dll

using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace CaryDN
{
    [ComVisible(true)]
    [Guid("31F0B69F-E4BC-4328-9B0C-1DFDE3330007")]
    [ProgId("CaryDN.AccessoryDataEventArgs")]
    [ClassInterface(ClassInterfaceType.None)]
    public class AccessoryDataEventArgs : CaryMessageEventArgs, IAccessoryDataEventArgs
    {
        #region Fields

        #endregion

        #region Constructors

        public AccessoryDataEventArgs(Message message)
            : base(message)
        {
            Device = WParam;
            Value = LParamLO;
            switch (LParamHi)
            {
                case 0:
                    Response = AccessoryResponse.Status;
                    break;
                case 1:
                    Response = AccessoryResponse.Reply;
                    break;
                case 2:
                    Response = AccessoryResponse.Monitor;
                    break;
                default:
                    Response = AccessoryResponse.Unknown;
                    break;
            }
        }

        #endregion

        #region Properties, Indexers

        public int Device { get; set; }

        public AccessoryResponse Response { get; set; }

        public int Value { get; set; }

        #endregion

        #region Interface Implementations

        public override string ToString()
        {
            return $"Device={Device}, Response={Response}, Value={Value}";
        }

        #endregion
    }
}