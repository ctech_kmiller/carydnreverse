﻿// Decompiled with JetBrains decompiler
// Type: CaryDN.ErrorCode
// Assembly: CaryDN, Version=1.0.6.0, Culture=neutral, PublicKeyToken=null
// MVID: D1AE64FC-DE05-4F0D-A2F5-6382B4200B36
// Assembly location: C:\src\Decompile\Agilent\CaryDN\1.0.6.0\Decomp\CaryDN.dll

using System.Runtime.InteropServices;
using CaryDN.Helpers;

namespace CaryDN
{
    [ComVisible(false)]
    public class ErrorCode
    {
        #region Static Fields and Constants

        // ReSharper disable once InconsistentNaming
        public const string K_RESOURCE_ID_PREFIX = "E";

        #endregion

        #region All other members

        public static string GetPrefix(int errorCode)
        {
            if (IsBetween(9000, 9099, errorCode))
                return "REX";
            if (IsBetween(9201, 9241, errorCode))
                return "MSG";
            if (IsBetween(9300, 9399, errorCode))
                return "SEQ";
            if (IsBetween(9400, 9499, errorCode))
                return "CHG";
            if (IsBetween(9500, 9599, errorCode))
                return "SIG";
            if (IsBetween(9600, 9699, errorCode))
                return "NMP";
            if (IsBetween(9700, 9799, errorCode))
                return "MTH";
            if (IsBetween(9800, 9899, errorCode))
                return "CCT";
            return string.Empty;
        }

        /// <summary>
        ///     Return true if <see cref="testValue" /> is between <see cref="lowLimit" /> and <see cref="highLimit" /> inclusive
        /// </summary>
        /// <param name="lowLimit"></param>
        /// <param name="highLimit"></param>
        /// <param name="testValue"></param>
        /// <returns></returns>
        private static bool IsBetween(int lowLimit, int highLimit, int testValue)
        {
            return testValue >= lowLimit && testValue <= highLimit;
        }

        public static string ToResourceID(int errorCode)
        {
            return $"E{errorCode:0###}";
        }

        public static string ToString(int errorCode)
        {
            var prefix = GetPrefix(errorCode);
            var str = ResourceHelper.GetString(ToResourceID(errorCode));
            if (string.IsNullOrEmpty(str))
                str = $"Cary error '{errorCode}'";
            return $"{prefix}:{str}";
        }

        #endregion
    }
}