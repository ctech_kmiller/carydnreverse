﻿// Decompiled with JetBrains decompiler
// Type: CaryDN.CaryModel
// Assembly: CaryDN, Version=1.0.6.0, Culture=neutral, PublicKeyToken=null
// MVID: D1AE64FC-DE05-4F0D-A2F5-6382B4200B36
// Assembly location: C:\src\Decompile\Agilent\CaryDN\1.0.6.0\Decomp\CaryDN.dll

// ReSharper disable InconsistentNaming

namespace CaryDN
{
    public enum CaryModel
    {
        Unknown = -1,
        Cary50 = 0,
        Cary100 = 1,
        Cary200 = 2,
        Cary300 = 3,
        Cary400 = 4,
        Cary500 = 5,
        Cary4000 = 6,
        Cary5000 = 7,
        Cary6000i = 8,
        DeepUV = 9,
        Cary60 = 10 // 0x0000000A
    }
}