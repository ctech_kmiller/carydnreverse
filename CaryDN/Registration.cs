﻿// Decompiled with JetBrains decompiler
// Type: CaryDN.Registration
// Assembly: CaryDN, Version=1.0.6.0, Culture=neutral, PublicKeyToken=null
// MVID: D1AE64FC-DE05-4F0D-A2F5-6382B4200B36
// Assembly location: C:\src\Decompile\Agilent\CaryDN\1.0.6.0\Decomp\CaryDN.dll

using System;
using System.ComponentModel;
using System.Runtime.InteropServices;
using Microsoft.Win32;

namespace CaryDN
{
    [ClassInterface(ClassInterfaceType.AutoDual)]
    [ComVisible(true)]
    [Guid("31F0B69F-E4BC-4328-9B0C-1DFDE3330004")]
    [ProgId("CaryDN.Registration")]
    [ComDefaultInterface(typeof(IRegistration))]
    public class Registration : IRegistration
    {
        #region Fields

        // ReSharper disable once UnusedMember.Local
        private DateTime _dateTime1 = DateTime.MinValue;
        // ReSharper disable once ConvertToConstant.Local
        private readonly string _registrationKeyRegistryKeyName = "Software\\microsoft\\WinMgrPlusEx\\Marlinka";

        #endregion

        #region Constructors

        public Registration()
        {
            CheckForTrialStartExists();
        }

        #endregion

        #region Properties, Indexers

        public string Key { get; set; }
        public int TrialDays { get; } = 100;

        [DisplayName("Trial Days Left")]
        [Description("The number of trial days remaining.")]
        public int TrialDaysLeft
        {
            get
            {
                if (TrialDaysUsed > TrialDays || TrialDaysUsed == TrialDays)
                    return 0;
                return TrialDays - TrialDaysUsed;
            }
        }

        public int TrialDaysUsed => TimeSpan.FromTicks(DateTime.Now.Ticks - TrialStart.Ticks).Days;

        public DateTime TrialEnd => TrialStart.AddDays(TrialDays);

        public DateTime TrialStart { get; private set; } = DateTime.MinValue;

        #endregion

        #region Interface Implementations

        public void Load()
        {
        }

        public override string ToString()
        {
            return $"{TrialDaysLeft} Days Left";
        }

        #endregion

        #region All other members

        /// <summary>
        ///     Checks registry to see if trial started and if not, create key with current DateTime.
        /// </summary>
        private void CheckForTrialStartExists()
        {
            var registryKey1 = openRegistryKey();
            if (registryKey1 == null)
            {
                TrialStart = DateTime.Now;
                var registryKey2 = createRegistryKey();
                registryKey2.SetValue("TrialStart", TrialStart.Ticks);
                registryKey2.Close();
            }
            else
            {
                TrialStart = new DateTime(Convert.ToInt64(registryKey1.GetValue("TrialStart")));
                registryKey1.Close();
            }
        }

        // ReSharper disable once InconsistentNaming
        private RegistryKey createRegistryKey()
        {
            return Registry.CurrentUser.CreateSubKey(_registrationKeyRegistryKeyName);
        }

        // ReSharper disable once UnusedMember.Local
        // ReSharper disable once InconsistentNaming
        private void deleteRegistryKey()
        {
            Registry.CurrentUser.DeleteSubKey(_registrationKeyRegistryKeyName);
        }

        // ReSharper disable once InconsistentNaming
        private RegistryKey openRegistryKey()
        {
            return Registry.CurrentUser.OpenSubKey(_registrationKeyRegistryKeyName);
        }

        #endregion
    }
}