﻿// Decompiled with JetBrains decompiler
// Type: CaryDN.Helpers.ResourceHelper
// Assembly: CaryDN, Version=1.0.6.0, Culture=neutral, PublicKeyToken=null
// MVID: D1AE64FC-DE05-4F0D-A2F5-6382B4200B36
// Assembly location: C:\src\Decompile\Agilent\CaryDN\1.0.6.0\Decomp\CaryDN.dll

using System;
using System.Reflection;
using System.Resources;
using System.Runtime.InteropServices;

namespace CaryDN.Helpers
{
    [ComVisible(false)]
    public class ResourceHelper
    {
        #region All other members

        public static string GetDllName()
        {
            return GetDllName(Assembly.GetExecutingAssembly());
        }

        public static string GetDllName(Type type)
        {
            return GetDllName(Assembly.GetAssembly(type));
        }

        public static string GetDllName(Assembly assembly)
        {
            return assembly.FullName.Split(',')[0];
        }

        public static string GetString(string resourceID)
        {
            return GetString(resourceID, "ResourcesEN-AU");
        }

        public static string GetString(string resourceID, string resourceFileName)
        {
            var executingAssembly = Assembly.GetExecutingAssembly();
            return GetString(string.Format("{0}.Resources.{1}", GetDllName(executingAssembly), resourceFileName),
                executingAssembly, resourceID);
        }

        public static string GetString(string baseName, Assembly assembly, string resourceID)
        {
            var str = new ResourceManager(baseName, assembly)
            {
                IgnoreCase = true
            }.GetString(resourceID);
            if (string.IsNullOrEmpty(str))
                return resourceID;
            return str;
        }

        public static bool ResourceExists(string resourceName)
        {
            return Assembly.GetExecutingAssembly().GetManifestResourceInfo(resourceName) != null;
        }

        public static bool ResourceExists(Type type, string resourceName)
        {
            return ResourceExists(Assembly.GetAssembly(type), resourceName);
        }

        public static bool ResourceExists(Assembly assembly, string resourceName)
        {
            return assembly.GetManifestResourceInfo(resourceName) != null;
        }

        #endregion
    }
}