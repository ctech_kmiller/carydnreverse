﻿// Decompiled with JetBrains decompiler
// Type: Class2
// Assembly: CaryDN, Version=1.0.6.0, Culture=neutral, PublicKeyToken=null
// MVID: D1AE64FC-DE05-4F0D-A2F5-6382B4200B36
// Assembly location: C:\src\Decompile\Agilent\CaryDN\1.0.6.0\Decomp\CaryDN.dll

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using System.Text;

// ReSharper disable MemberHidesStaticFromOuterClass
// ReSharper disable RedundantNameQualifier
#pragma warning disable 252,253

// ReSharper disable once CheckNamespace
internal static class Class2
{
    #region Static Fields and Constants

    // ReSharper disable once FieldCanBeMadeReadOnly.Local
    private static Class3 _class3 = new Class3();

    #endregion

    #region All other members

    internal static long smethod_0()
    {
        if (Assembly.GetCallingAssembly() != (object) typeof(Class2).Assembly || !smethod_1())
            return 0;
        lock (_class3)
        {
            var long0 = _class3.method_0();
            // ReSharper disable once InvertIf
            if (long0 == 0L)
            {
                var executingAssembly = Assembly.GetExecutingAssembly();
                var byteList = new List<byte>();
                AssemblyName assemblyName;
                try
                {
                    assemblyName = executingAssembly.GetName();
                }
                catch
                {
                    assemblyName = new AssemblyName(executingAssembly.FullName);
                }

                var numArray = assemblyName.GetPublicKeyToken();
                if (numArray != null && numArray.Length == 0)
                    numArray = null;
                if (numArray != null)
                    byteList.AddRange(numArray);
                byteList.AddRange(Encoding.Unicode.GetBytes(assemblyName.Name));
                var num1 = smethod_3(typeof(Class2));
                var num2 = Class7.smethod_0();
                byteList.Add((byte) (num1 >> 24));
                byteList.Add((byte) (num2 >> 16));
                byteList.Add((byte) (num1 >> 8));
                byteList.Add((byte) num2);
                byteList.Add((byte) (num1 >> 16));
                byteList.Add((byte) (num2 >> 8));
                byteList.Add((byte) num1);
                byteList.Add((byte) (num2 >> 24));
                var count = byteList.Count;
                ulong num3 = 0;
                for (var index = 0; index != count; ++index)
                {
                    var num4 = num3 + byteList[index];
                    var num5 = num4 + (num4 << 20);
                    num3 = num5 ^ (num5 >> 12);
                    byteList[index] = 0;
                }

                var num6 = num3 + (num3 << 6);
                var num7 = num6 ^ (num6 >> 22);
                long0 = (long) (num7 + (num7 << 30)) ^ -7220120030393307105L;
                _class3.method_1(long0);
            }

            return long0;
        }
    }

    private static bool smethod_1()
    {
        return smethod_2();
    }

    private static bool smethod_2()
    {
        var frame = new StackTrace().GetFrame(3);
        var methodBase = frame?.GetMethod();
        var type = (object) methodBase == null ? null : methodBase.DeclaringType;
        return type != (object) typeof(RuntimeMethodHandle) && (object) type != null &&
               type.Assembly == (object) typeof(Class2).Assembly;
    }

    private static int smethod_3(Type type0)
    {
        return type0.MetadataToken;
    }

    #endregion

    #region Nested Types

    private sealed class Class3
    {
        #region Fields

        private int _int0;
        private int _int1;

        #endregion

        #region Constructors

        internal Class3()
        {
            method_1(0L);
        }

        #endregion

        #region All other members

        internal long method_0()
        {
            if (Assembly.GetCallingAssembly() != (object) typeof(Class3).Assembly || !smethod_1())
                return 2918384;
            var numArray = new[] {0, 0, 0, 664309688};
            numArray[1] = -276178962;
            numArray[2] = -419927488;
            numArray[0] = 339826390;
            var int0 = _int0;
            var int1 = _int1;
            var num1 = -1640531527;
            var num2 = -957401312;
            for (var index = 0; index != 32; ++index)
            {
                int1 -= (((int0 << 4) ^ (int0 >> 5)) + int0) ^ (num2 + numArray[(num2 >> 11) & 3]);
                num2 -= num1;
                int0 -= (((int1 << 4) ^ (int1 >> 5)) + int1) ^ (num2 + numArray[num2 & 3]);
            }

            for (var index = 0; index != 4; ++index)
                numArray[index] = 0;
            return (long) (((ulong) int1 << 32) | (uint) int0);
        }

        internal void method_1(long long0)
        {
            if (Assembly.GetCallingAssembly() != (object) typeof(Class3).Assembly || !smethod_1())
                return;
            var numArray = new[] {0, -276178962, 0, 0};
            numArray[0] = 339826390;
            numArray[2] = -419927488;
            numArray[3] = 664309688;
            var num1 = -1640531527;
            var num2 = (int) long0;
            var num3 = (int) (long0 >> 32);
            var num4 = 0;
            for (var index = 0; index != 32; ++index)
            {
                num2 += (((num3 << 4) ^ (num3 >> 5)) + num3) ^ (num4 + numArray[num4 & 3]);
                num4 += num1;
                num3 += (((num2 << 4) ^ (num2 >> 5)) + num2) ^ (num4 + numArray[(num4 >> 11) & 3]);
            }

            for (var index = 0; index != 4; ++index)
                numArray[index] = 0;
            _int0 = num2;
            _int1 = num3;
        }

        #endregion
    }

    private sealed class Class4
    {
        #region All other members

        internal static int smethod_0()
        {
            return Class5.smethod_2(Class5.smethod_0(Class9.smethod_0() ^ 527758446, smethod_3(typeof(Class10))),
                Class5.smethod_1(smethod_3(typeof(Class7)) ^ smethod_3(typeof(Class6)), -1542172401));
        }

        #endregion
    }

    private static class Class5
    {
        #region All other members

        internal static int smethod_0(int int0, int int1)
        {
            return int0 ^ (int1 - 882028653);
        }

        internal static int smethod_1(int int0, int int1)
        {
            return (int0 - -1473858907) ^ (int1 - 262711273);
        }

        internal static int smethod_2(int int0, int int1)
        {
            return int0 ^ (int1 - 453448525) ^ (int0 - int1);
        }

        #endregion
    }

    private sealed class Class6
    {
        #region All other members

        internal static int smethod_0()
        {
            return Class5.smethod_0(smethod_3(typeof(Class6)),
                Class5.smethod_2(Class5.smethod_1(smethod_3(typeof(Class8)), smethod_3(typeof(Class7))),
                    Class5.smethod_2(smethod_3(typeof(Class4)) ^ 1228802992, Class8.smethod_0())));
        }

        #endregion
    }

    private sealed class Class7
    {
        #region All other members

        internal static int smethod_0()
        {
            return Class5.smethod_2(
                Class5.smethod_1(smethod_3(typeof(Class9)),
                    Class5.smethod_2(smethod_3(typeof(Class7)), smethod_3(typeof(Class8)))), Class6.smethod_0());
        }

        #endregion
    }

    private sealed class Class8
    {
        #region All other members

        internal static int smethod_0()
        {
            return Class5.smethod_1(
                Class5.smethod_1(Class4.smethod_0(), Class5.smethod_0(smethod_3(typeof(Class8)), Class9.smethod_0())),
                smethod_3(typeof(Class6)));
        }

        #endregion
    }

    private sealed class Class9
    {
        #region All other members

        internal static int smethod_0()
        {
            return Class5.smethod_0(smethod_3(typeof(Class4)),
                smethod_3(typeof(Class10)) ^ Class5.smethod_1(smethod_3(typeof(Class9)),
                    Class5.smethod_2(smethod_3(typeof(Class6)), Class10.smethod_0())));
        }

        #endregion
    }

    private sealed class Class10
    {
        #region All other members

        internal static int smethod_0()
        {
            return Class5.smethod_2(smethod_3(typeof(Class10)),
                Class5.smethod_0(smethod_3(typeof(Class7)),
                    Class5.smethod_1(smethod_3(typeof(Class9)),
                        Class5.smethod_2(smethod_3(typeof(Class4)),
                            Class5.smethod_0(smethod_3(typeof(Class8)), smethod_3(typeof(Class6)))))));
        }

        #endregion
    }

    #endregion
}