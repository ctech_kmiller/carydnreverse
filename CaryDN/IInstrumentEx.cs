﻿// Decompiled with JetBrains decompiler
// Type: CaryDN.IInstrumentEx
// Assembly: CaryDN, Version=1.0.6.0, Culture=neutral, PublicKeyToken=null
// MVID: D1AE64FC-DE05-4F0D-A2F5-6382B4200B36
// Assembly location: C:\src\Decompile\Agilent\CaryDN\1.0.6.0\Decomp\CaryDN.dll

using System.ComponentModel;
using System.Runtime.InteropServices;

namespace CaryDN
{
    [ComVisible(true)]
    [InterfaceType(ComInterfaceType.InterfaceIsDual)]
    [Guid("31F0B69F-E4BC-4328-9B0C-2DFDE3330000")]
    public interface IInstrumentEx
    {
        float AbscissaInterval { get; set; }

        float AbscissaStart { get; set; }

        int Averaging { get; set; }

        BeamMode BeamMode { get; set; }

        CalibrationMode CallibrationMode { get; set; }

        float CaryReading(float wavelength);

        bool CaryTest(InstrumentParameterID id, int value);

        int ChopperCycles { get; }

        float CollectPending { get; set; }

        bool Connect();

        void Connect(int timeOut, bool simulatorMode);

        int ConnectionTimeout { get; set; }

        DetectorMode CurrentDetector { get; }

        int CurrentFilter { get; }

        GratingMode CurrentGrating { get; }

        int CurrentSource { get; }

        DetectorMode Detector { get; }

        float DetectorChangeoverWavelength { get; set; }

        void Disconnect();

        void Dispose();

        int Filter { get; set; }

        int Gain { get; set; }

        void GetDefaultSetup();

        string GetErrorMessage(int errorCode);

        void GetModel();

        void GetStatus();

        void GotoWavelength(float wavelength);

        float GratingChangeoverWavelength { get; set; }

        GratingMode GratingMode { get; set; }

        float InstrumentTime { get; set; }

        bool IsBusy { get; }

        bool IsCollecting { get; }

        bool IsConnected { get; }

        bool IsOffLine { get; }

        bool IsOnline { get; }

        bool IsResetting { get; }

        bool IsScanning { get; }

        bool IsThirdLampOn { get; set; }

        bool IsUvLampOn { get; set; }

        bool IsVisibleLampOn { get; set; }

        float Lamp1Time { get; set; }

        float Lamp2Time { get; set; }

        float Lamp3Time { get; set; }

        float LastD2PeakWavelength { get; }

        float LastPmvGainIndex { get; }

        float LastReadValue { get; }

        float LastRearTValue { get; }

        float LastSlitWidth { get; }

        float LastTime { get; }

        float LastTransmissionValue { get; }

        CaryModel Model { get; }

        int NIRAveraging { get; set; }

        float NIRDetectorCalibration { get; }

        float NIRInterval { get; set; }

        float NIRPeakWavelengths { get; set; }

        float NIRRefLevel { get; set; }

        float NIRSlitWidth { get; set; }

        float NIRWavelengthCalibA { get; set; }

        float NIRWavelengthCalibB { get; set; }

        float NIRWavelengthCalibC { get; set; }

        float PgaGainIndicesDark { get; set; }

        float PgaGainIndicesFront { get; set; }

        float PgaGainIndicesRear { get; set; }

        PGAMode PgaMode { get; set; }

        event PropertyChangedEventHandler PropertyChanged;

        void Read(out float wavelength, out float frontBeam, out float rearBeam);

        float Read(float wavelength);

        void RemoveDeviceResponse(DeviceAddress device, uint status);

        ScanMode ScanMode { get; set; }

        float ScanPoints { get; set; }

        float ScanStart { get; set; }

        float ScanStop { get; set; }

        void SetCommsDebug(bool onOff);

        void SetSimFile(string fileName);

        void Setup();

        float ShutterFront { get; }

        float ShutterRear { get; }

        float ShuttersOpenClosedFront { get; }

        float ShuttersOpenClosedRear { get; }

        float SigProccCalibGain { get; set; }

        float SigProccCalibOffset { get; set; }

        bool SimulatorMode { get; set; }

        SlitHeight SlitHeight { get; set; }

        float SourceChangeoverWavelength { get; set; }

        SourceMode SourceMode { get; set; }

        void Start();

        float Status { get; }

        void Stop();

        void SubSetup(SubSetupCode code);

        float TargetSignalNoise { get; set; }

        float Timer { get; set; }

        int UvVisAveraging { get; set; }

        float UvVisInterval { get; set; }

        float UvVisSlitWidth { get; set; }

        float UvVisWaveNumberInterval { get; set; }

        float VersionMain { get; }

        void WaitNotBusy();

        void WaitNotScanning();

        float Wavelength { get; set; }

        int WindowHandle { get; set; }

        Accessory Accessory { get; }

        Registration Registration { get; }
    }
}