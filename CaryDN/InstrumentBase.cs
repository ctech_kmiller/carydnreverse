﻿// Decompiled with JetBrains decompiler
// Type: CaryDN.InstrumentBase
// Assembly: CaryDN, Version=1.0.6.0, Culture=neutral, PublicKeyToken=null
// MVID: D1AE64FC-DE05-4F0D-A2F5-6382B4200B36
// Assembly location: C:\src\Decompile\Agilent\CaryDN\1.0.6.0\Decomp\CaryDN.dll

using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;
using CaryDN.SCary32;
using System.Runtime.ExceptionServices;
using System.Security;

// ReSharper disable ArrangeAccessorOwnerBody

namespace CaryDN
{
    [ComVisible(false)]
    public class InstrumentBase : IDisposable, INotifyPropertyChanged
    {
        #region Public Delegates

        public delegate void Cary32MessageHandler();

        #endregion

        #region Static Fields and Constants

        // ReSharper disable once InconsistentNaming
        public const int DB_OFF = 0;

        // ReSharper disable once InconsistentNaming
        public const int DB_ON = 1;

        #endregion

        #region Fields

        private readonly Cary32MessageHandler _cary32MessageHandler0;

        #endregion

        #region Constructors

        internal InstrumentBase()
        {
            _cary32MessageHandler0 = DoApplicationEvents;
            Debug.WriteLine($"BaseWindowHandle{this.WindowHandle}");
        }

        #endregion

        #region Properties, Indexers

        [DisplayName("Abscissa Interval")]
        [Description("")]
        [Category("Misc")]
        public float AbscissaInterval
        {
            get { return GetCaryParameterValue(InstrumentParameterID.RESP_ABSCISSA_INTERVAL); }
            set { TryCarySetParameterValue(InstrumentParameterID.RESP_ABSCISSA_INTERVAL, value); }
        }

        [Category("Misc")]
        [Description("")]
        [DisplayName("Abscissa Start")]
        public float AbscissaStart
        {
            get { return GetCaryParameterValue(InstrumentParameterID.RESP_ABSCISSA_START); }
            set { TryCarySetParameterValue(InstrumentParameterID.RESP_ABSCISSA_START, value); }
        }

        [Description("The average read time in milli seconds.")]
        [Category("UV Visible")]
        [DisplayName("Adjusted Averaging")]
        public int Averaging
        {
            get
            {
                return (int) (float) Math.Round(GetCaryParameterValue(InstrumentParameterID.RESP_UVVIS_AVERAGING) /
                                                (double) ChopperCycles * 1000.0);
            }
            set
            {
                // ReSharper disable once PossibleLossOfFraction
                float float0 = value * ChopperCycles / 1000;
                if (float0 < 1.0)
                    float0 = 1f;
                TryCarySetParameterValue(InstrumentParameterID.RESP_UVVIS_AVERAGING, float0);
            }
        }

        [Category("Lamps")]
        [DisplayName("Beam Mode")]
        [Description("BeamMode controls the instrument’s beam arrangement.")]
        [DefaultValue(4)]
        public BeamMode BeamMode
        {
            get { return (BeamMode) GetCaryParameterValueAsInt(InstrumentParameterID.RESP_BEAM_MODE); }
            set { TryCarySetParameterValue(InstrumentParameterID.RESP_BEAM_MODE, (float) value); }
        }

        [Category("Misc")]
        [Description("")]
        [DisplayName("Callibration Mode")]
        public CalibrationMode CallibrationMode
        {
            get { return (CalibrationMode) GetCaryParameterValueAsInt(InstrumentParameterID.RESP_CAL_MODE); }
            set { TryCarySetParameterValue(InstrumentParameterID.RESP_CAL_MODE, (float) value); }
        }

        [DisplayName("Chopper Cycles")]
        [Description("")]
        [Category("Misc")]
        public int ChopperCycles
        {
            get { return Model == CaryModel.Cary50 || Model == CaryModel.Cary60 ? 80 : 30; }
        }

        [Description("")]
        [Category("Misc")]
        [DisplayName("Collect Pending")]
        public float CollectPending
        {
            get { return GetCaryParameterValue(InstrumentParameterID.RESP_COLLECT_PENDING); }
            set { TryCarySetParameterValue(InstrumentParameterID.RESP_COLLECT_PENDING, value); }
        }

        [Description("The timeout (milli sec) used when connecting to the Cary.")]
        public int ConnectionTimeout { get; set; } = 500;

        [Category("Detector")]
        [Description("")]
        [DisplayName("Current Detector")]
        public DetectorMode CurrentDetector
        {
            get { return (DetectorMode) GetCaryParameterValueAsInt(InstrumentParameterID.RESP_CURRENT_DETECTOR); }
        }

        [DisplayName("Current Filter")]
        [Category("Filter")]
        [Description("")]
        public int CurrentFilter
        {
            get { return GetCaryParameterValueAsInt(InstrumentParameterID.RESP_CURRENT_FILTER); }
        }

        [DisplayName("Current Grating")]
        [Description("")]
        [Category("Grating")]
        public GratingMode CurrentGrating
        {
            get { return (GratingMode) GetCaryParameterValueAsInt(InstrumentParameterID.RESP_CURRENT_GRATING); }
        }

        [Description("The current light source.")]
        [DisplayName("Current Source")]
        [Category("Misc")]
        public int CurrentSource
        {
            get { return GetCaryParameterValueAsInt(InstrumentParameterID.RESP_CURRENT_SOURCE); }
        }

        [Category("Detector")]
        [DisplayName("Detector")]
        [Description("")]
        public DetectorMode Detector
        {
            get { return (DetectorMode) GetCaryParameterValueAsInt(InstrumentParameterID.RESP_DETECTOR); }
        }

        [DisplayName("Detector Changeover")]
        [DefaultValue(900)]
        [Category("Detector")]
        [Description("This is the detector changeover wavelength. Default value is 900 nm")]
        public float DetectorChangeoverWavelength
        {
            get { return GetCaryParameterValueAsInt(InstrumentParameterID.RESP_DETECTOR_CHANGEOVER_WAVELENGTH); }
            set { TryCarySetParameterValue(InstrumentParameterID.RESP_DETECTOR_CHANGEOVER_WAVELENGTH, value); }
        }

        [Description("")]
        [Category("Filter")]
        [DisplayName("Filter")]
        public int Filter
        {
            get { return GetCaryParameterValueAsInt(InstrumentParameterID.RESP_FILTER); }
            set { TryCarySetParameterValue(InstrumentParameterID.RESP_FILTER, value); }
        }

        [Description("")]
        [DisplayName("Gain")]
        [Category("Detector")]
        public int Gain
        {
            get { return GetCaryParameterValueAsInt(InstrumentParameterID.RESP_GAIN); }
            set { TryCarySetParameterValue(InstrumentParameterID.RESP_GAIN, value); }
        }

        [DisplayName("Grating Changeover")]
        [Description("This is the grating changeover wavelength.")]
        [Category("Grating")]
        public float GratingChangeoverWavelength
        {
            get { return GetCaryParameterValue(InstrumentParameterID.RESP_GRATING_CHANGEOVER_WAVELENGTH); }
            set { TryCarySetParameterValue(InstrumentParameterID.RESP_GRATING_CHANGEOVER_WAVELENGTH, value); }
        }

        [Category("Grating")]
        [Description("This is the instrument parameter RESP_GRATING")]
        [DisplayName("Grating Mode")]
        public GratingMode GratingMode
        {
            get { return (GratingMode) GetCaryParameterValueAsInt(InstrumentParameterID.RESP_GRATING); }
            set { TryCarySetParameterValue(InstrumentParameterID.RESP_GRATING, (float) value); }
        }

        [DisplayName("Instrument Time")]
        [Description("This is the instrument parameter RESP_INSTRUMENT_TIME")]
        [Category("Misc")]
        public float InstrumentTime
        {
            get { return GetCaryParameterValue(InstrumentParameterID.RESP_INSTRUMENT_TIME); }
            set { TryCarySetParameterValue(InstrumentParameterID.RESP_INSTRUMENT_TIME, value); }
        }

        [Description("Indicates if the instrument is busy setting up or collecting data.")]
        [Category("Status")]
         public bool IsBusy
        {
            get
            {
                if (!IsConnected)
                    return false;
                return Scary.CaryBusy();
            }
        }

        [Description("This is the instrument parameter RESP_COLLECTING")]
        [Category("Status")]
        [DisplayName("IsCollecting")]
        public bool IsCollecting
        {
            get { return IsParameterValueZero(InstrumentParameterID.RESP_COLLECTING); }
        }

        [Description("")] [Category("Status")] public bool IsConnected { get; private set; }

        [Category("Status")]
        [Description("This is the instrument parameter RESP_OFFLINE")]
        [DisplayName("IsOffLine")]
        public bool IsOffLine
        {
            get { return IsParameterValueZero(InstrumentParameterID.RESP_OFFLINE); }
        }

        [Description("This property tracks the UM_CARY_OFFLINE, UM_CARY_ONLINE messages.")]
        [Category("Status")]
        public bool IsOnline { get; private set; }

        [Description(
            "This is the instrument parameter RESP_RESETTING. This is true after power on whilst the Cary is resetting.")]
        [DisplayName("IsResetting")]
        [Category("Status")]
        public bool IsResetting
        {
            get { return IsParameterValueZero(InstrumentParameterID.RESP_RESETTING); }
        }

        [Description("Returns the instrument scanning status.")]
        [Category("Status")]
        public bool IsScanning
        {
            get
            {
                if (!IsConnected)
                    return false;
                return Scary.CaryScanning();
            }
        }

        [DisplayName("Third Lamp On")]
        [Category("Lamps")]
        [Description("This is the status of the third lamp.")]
        public bool IsThirdLampOn
        {
            get { return CaryTest(InstrumentParameterID.RESP_SOURCE_ON_OFF_THIRD, 1); }
            set
            {
                if (value)
                    TryCarySetParameterValue(InstrumentParameterID.RESP_SOURCE_ON_OFF_THIRD, 1f);
                else
                    TryCarySetParameterValue(InstrumentParameterID.RESP_SOURCE_ON_OFF_THIRD, 0.0f);
            }
        }

        [DisplayName("UV Lamp On")]
        [Category("Lamps")]
        [Description("This is the status of the Ultra Violet lamp.")]
        public bool IsUvLampOn
        {
            get { return CaryTest(InstrumentParameterID.RESP_SOURCE_ON_OFF_UV, 1); }
            set
            {
                if (value)
                    TryCarySetParameterValue(InstrumentParameterID.RESP_SOURCE_ON_OFF_UV, 1f);
                else
                    TryCarySetParameterValue(InstrumentParameterID.RESP_SOURCE_ON_OFF_UV, 0.0f);
            }
        }

        [Category("Lamps")]
        [Description("This is the status of the visible light spectrum lamp.")]
        [DisplayName("VIS Lamp On")]
        public bool IsVisibleLampOn
        {
            get { return CaryTest(InstrumentParameterID.RESP_SOURCE_ON_OFF_VIS, 1); }
            set
            {
                if (value)
                    TryCarySetParameterValue(InstrumentParameterID.RESP_SOURCE_ON_OFF_VIS, 1f);
                else
                    TryCarySetParameterValue(InstrumentParameterID.RESP_SOURCE_ON_OFF_VIS, 0.0f);
            }
        }

        [DisplayName("UV Lamp Time")]
        [Description("This is also named Lamp1Time.")]
        [Category("Lamps")]
        public float Lamp1Time
        {
            get { return GetCaryParameterValue(InstrumentParameterID.RESP_LAMP1_TIME); }
            set { TryCarySetParameterValue(InstrumentParameterID.RESP_LAMP1_TIME, value); }
        }

        [DisplayName("VIS Lamp Time")]
        [Description("The Visible lamp time. This is also named Lamp2Time.")]
        [Category("Lamps")]
        public float Lamp2Time
        {
            get { return GetCaryParameterValue(InstrumentParameterID.RESP_LAMP2_TIME); }
            set { TryCarySetParameterValue(InstrumentParameterID.RESP_LAMP2_TIME, value); }
        }

        [DisplayName("Third Lamp Time")]
        [Category("Lamps")]
        [Description("This is also named Lamp3Time.")]
        public float Lamp3Time
        {
            get { return GetCaryParameterValue(InstrumentParameterID.RESP_LAMP3_TIME); }
            set { TryCarySetParameterValue(InstrumentParameterID.RESP_LAMP3_TIME, value); }
        }

        [Category("Lamps")]
        [Description("")]
        [DisplayName("Last D2 Peak")]
        public float LastD2PeakWavelength
        {
            get { return GetCaryParameterValue(InstrumentParameterID.RESP_LAST_D2PEAK_WAVELENGTH); }
        }

        [Description("DAC output controlling the EHT voltage on the PMT, 0..255")]
        [Category("Detector")]
        [DisplayName("Last Pmv Gain Index")]
        public float LastPmvGainIndex
        {
            get { return GetCaryParameterValue(InstrumentParameterID.RESP_LAST_PMV_GAIN_INDEX); }
        }

        [Category("Misc")]
        [Description("")]
        [DisplayName("Last read value")]
        public float LastReadValue
        {
            get { return GetCaryParameterValue(InstrumentParameterID.RESP_LAST_RD_VALUE); }
        }

        [DisplayName("LastRearTValue")]
        [Description("Last Rear Raw T value")]
        [Category("Misc")]
        public float LastRearTValue
        {
            get { return GetCaryParameterValue(InstrumentParameterID.RESP_LAST_REART_VALUE); }
        }

        [Description("")]
        [DisplayName("Last slit width")]
        [Category("Slit")]
        public float LastSlitWidth
        {
            get { return GetCaryParameterValue(InstrumentParameterID.RESP_LAST_SLIT_WIDTH); }
        }

        [Category("Misc")]
        [Description("")]
        [DisplayName("Last Time")]
        public float LastTime
        {
            get { return GetCaryParameterValue(InstrumentParameterID.RESP_LAST_TIME); }
        }

        [Description("")]
        [Category("Misc")]
        [DisplayName("Last Transmission Value")]
        public float LastTransmissionValue
        {
            get { return GetCaryParameterValue(InstrumentParameterID.RESP_LAST_TRANSMISSION_VALUE); }
        }

        [Description("The instrument model number.")]
        [DisplayName("Model")]
        [Category("Misc")]
        public CaryModel Model
        {
            get
            {
                var num = GetCaryParameterValueAsInt(InstrumentParameterID.RESP_MODEL);
                if (num < 0 || num > 10)
                    return CaryModel.Unknown;
                return (CaryModel) num;
            }
        }

        [Description(
            "NIRAveraging is the number of chopper cycles (1/30 s) over which the measurement should be averaged.")]
        [DisplayName("NIR Averaging")]
        [Category("Near Infra Red")]
        public int NIRAveraging
        {
            get
            {
                return (int) (float) Math.Round(
                    (float) (GetCaryParameterValue(InstrumentParameterID.RESP_NIR_AVERAGING) /
                             (double) ChopperCycles * 1000.0));
            }
            set
            {
                // ReSharper disable once PossibleLossOfFraction
                float float0 = value * ChopperCycles / 1000;
                if (float0 < 1.0)
                    float0 = 1f;
                TryCarySetParameterValue(InstrumentParameterID.RESP_NIR_AVERAGING, float0);
            }
        }

        [DisplayName("NIR Detector Calibration")]
        [Category("Near Infra Red")]
        [Description("")]
        public float NIRDetectorCalibration
        {
            get { return GetCaryParameterValue(InstrumentParameterID.RESP_NIR_DETECTOR_CALIBRATION); }
        }

        [DisplayName("NIR Interval")]
        [DefaultValue(0)]
        [Category("Near Infra Red")]
        [Description(
            "NIRInterval is the wavelength interval (nm) between consecutive points in an NIR wavelength scan.")]
        public float NIRInterval
        {
            get { return GetCaryParameterValue(InstrumentParameterID.RESP_NIR_INTERVAL); }
            set { TryCarySetParameterValue(InstrumentParameterID.RESP_NIR_INTERVAL, value); }
        }

        [Category("Near Infra Red")]
        [Description("")]
        [DisplayName("NIR Peak Wavelengths")]
        public float NIRPeakWavelengths
        {
            get { return GetCaryParameterValue(InstrumentParameterID.RESP_NIR_PEAK_WAVELENGTHS); }
            set { TryCarySetParameterValue(InstrumentParameterID.RESP_NIR_PEAK_WAVELENGTHS, value); }
        }

        [Description("")]
        [DisplayName("NIR Ref Level")]
        [Category("Near Infra Red")]
        public float NIRRefLevel
        {
            get { return GetCaryParameterValue(InstrumentParameterID.RESP_NIR_REF_LEVEL); }
            set { TryCarySetParameterValue(InstrumentParameterID.RESP_NIR_REF_LEVEL, value); }
        }

        [DefaultValue(4)]
        [Description("This is the slit width (nm) for an NIR scan. The default value is 4.")]
        [DisplayName("NIR Slit Width")]
        [Category("Near Infra Red")]
        public float NIRSlitWidth
        {
            get { return GetCaryParameterValue(InstrumentParameterID.RESP_NIR_SLIT_WIDTH); }
            set { TryCarySetParameterValue(InstrumentParameterID.RESP_NIR_SLIT_WIDTH, value); }
        }

        [Category("Near Infra Red")]
        [DisplayName("NIR Wavelength Calib A")]
        [Description("")]
        public float NIRWavelengthCalibA
        {
            get { return GetCaryParameterValue(InstrumentParameterID.RESP_NIR_WAVELENGTH_CALIB_A); }
            set { TryCarySetParameterValue(InstrumentParameterID.RESP_NIR_WAVELENGTH_CALIB_A, value); }
        }

        [DisplayName("NIR Wavelength Calib B")]
        [Category("Near Infra Red")]
        [Description("")]
        public float NIRWavelengthCalibB
        {
            get { return GetCaryParameterValue(InstrumentParameterID.RESP_NIR_WAVELENGTH_CALIB_B); }
            set { TryCarySetParameterValue(InstrumentParameterID.RESP_NIR_WAVELENGTH_CALIB_B, value); }
        }

        [Description("")]
        [Category("Near Infra Red")]
        [DisplayName("NIR Wavelength Calib C")]
        public float NIRWavelengthCalibC
        {
            get { return GetCaryParameterValue(InstrumentParameterID.RESP_NIR_WAVELENGTH_CALIB_C); }
            set { TryCarySetParameterValue(InstrumentParameterID.RESP_NIR_WAVELENGTH_CALIB_C, value); }
        }

        [DisplayName("PGA Gain Indices Dark")]
        [Description("")]
        [Category("PGA")]
        public float PgaGainIndicesDark
        {
            get { return GetCaryParameterValue(InstrumentParameterID.RESP_PGA_GAIN_INDICES_DARK); }
            set { TryCarySetParameterValue(InstrumentParameterID.RESP_PGA_GAIN_INDICES_DARK, value); }
        }

        [Description("")]
        [DisplayName("PGA Gain Indices Front")]
        [Category("PGA")]
        public float PgaGainIndicesFront
        {
            get { return GetCaryParameterValue(InstrumentParameterID.RESP_PGA_GAIN_INDICES_FRONT); }
            set { TryCarySetParameterValue(InstrumentParameterID.RESP_PGA_GAIN_INDICES_FRONT, value); }
        }

        [Category("PGA")]
        [Description("")]
        [DisplayName("PGA Gain Indices Rear")]
        public float PgaGainIndicesRear
        {
            get { return GetCaryParameterValue(InstrumentParameterID.RESP_PGA_GAIN_INDICES_REAR); }
            set { TryCarySetParameterValue(InstrumentParameterID.RESP_PGA_GAIN_INDICES_REAR, value); }
        }

        [Category("PGA")]
        [Description("Programable amplifier gain mode.")]
        [DisplayName("PGA Mode  ")]
        public PGAMode PgaMode
        {
            get { return (PGAMode) GetCaryParameterValueAsInt(InstrumentParameterID.RESP_PGA_MODE); }
            set { TryCarySetParameterValue(InstrumentParameterID.RESP_PGA_MODE, (float) value); }
        }

        [ReadOnly(true)]
        [Description("This reflects the instrument ScanMode only after the scan starts.")]
        [Category("Scan")]
        [DisplayName("Scan mode")]
        public ScanMode ScanMode
        {
            get { return (ScanMode) GetCaryParameterValueAsInt(InstrumentParameterID.RESP_SCAN_MODE); }
            set { TryCarySetParameterValue(InstrumentParameterID.RESP_SCAN_MODE, (float) value); }
        }

        [Category("Scan")]
        [DisplayName("Scan points")]
        [Description("")]
        public float ScanPoints
        {
            get { return GetCaryParameterValue(InstrumentParameterID.RESP_SCAN_POINTS); }
            set { TryCarySetParameterValue(InstrumentParameterID.RESP_SCAN_POINTS, value); }
        }

        [Description("")]
        [Category("Scan")]
        [DefaultValue(500)]
        [DisplayName("Scan Start")]
        public float ScanStart
        {
            get { return GetCaryParameterValue(InstrumentParameterID.RESP_SCAN_START); }
            set { SetCaryParameter(InstrumentParameterID.RESP_SCAN_START, value); }
        }

        [Description("")]
        [DisplayName("Scan Stop")]
        [Category("Scan")]
        public float ScanStop
        {
            get { return GetCaryParameterValue(InstrumentParameterID.RESP_SCAN_STOP); }
            set { SetCaryParameter(InstrumentParameterID.RESP_SCAN_STOP, value); }
        }

        [Description("")]
        [Category("Shutter")]
        [DisplayName("Shutter Front")]
        public float ShutterFront
        {
            get { return GetCaryParameterValue(InstrumentParameterID.RESP_SHUTTER_FRONT); }
        }

        [DisplayName("Shutter Rear")]
        [Description("")]
        [Category("Shutter")]
        public float ShutterRear
        {
            get { return GetCaryParameterValue(InstrumentParameterID.RESP_SHUTTER_REAR); }
        }

        [DisplayName("Shutters Open Closed Front")]
        [Category("Shutter")]
        [Description("Shutters Open Closed Front")]
        public float ShuttersOpenClosedFront
        {
            get { return GetCaryParameterValue(InstrumentParameterID.RESP_SHUTTERS_OPEN_CLOSED_FRONT); }
        }

        [Description("Shutters Open Closed Rear")]
        [DisplayName("Shutters Open Closed Rear")]
        [Category("Shutter")]
        public float ShuttersOpenClosedRear
        {
            get { return GetCaryParameterValue(InstrumentParameterID.RESP_SHUTTERS_OPEN_CLOSED_REAR); }
        }

        [Description("")]
        [DisplayName("Sig Procc Calib Gain")]
        [Category("Misc")]
        public float SigProccCalibGain
        {
            get { return GetCaryParameterValue(InstrumentParameterID.RESP_SIG_PROCC_CALIB_GAIN); }
            set { SetCaryParameter(InstrumentParameterID.RESP_SIG_PROCC_CALIB_GAIN, value); }
        }

        [DisplayName("Sig Procc Calib Offset")]
        [Description("")]
        [Category("Misc")]
        public float SigProccCalibOffset
        {
            get { return GetCaryParameterValue(InstrumentParameterID.RESP_SIG_PROCC_CALIB_OFFSET); }
            set { SetCaryParameter(InstrumentParameterID.RESP_SIG_PROCC_CALIB_OFFSET, value); }
        }

        [Category("Simulator")]
        [DisplayName("Simulator Mode")]
        public bool SimulatorMode { get; set; }

        [Description("")]
        [DisplayName("Slit Height")]
        [Category("Misc")]
        public SlitHeight SlitHeight
        {
            get { return (SlitHeight) GetCaryParameterValueAsInt(InstrumentParameterID.RESP_SLIT_HEIGHT); }
            // ReSharper disable once ValueParameterNotUsed
            set { SetCaryParameter(InstrumentParameterID.RESP_SLIT_HEIGHT, (float) SlitHeight); }
        }

        [Category("Lamps")]
        [DisplayName("Source Changeover")]
        [Description("This is the lamp source changeover wavelength.")]
        public float SourceChangeoverWavelength
        {
            get { return GetCaryParameterValue(InstrumentParameterID.RESP_SOURCE_CHANGEOVER_WAVELENGTH); }
            set { TryCarySetParameterValue(InstrumentParameterID.RESP_SOURCE_CHANGEOVER_WAVELENGTH, value); }
        }

        [DisplayName("Source Mode")]
        [Category("Lamps")]
        [Description("")]
        public SourceMode SourceMode
        {
            get { return (SourceMode) GetCaryParameterValueAsInt(InstrumentParameterID.RESP_SOURCE); }
            set { SetCaryParameter(InstrumentParameterID.RESP_SOURCE, (float) value); }
        }

        [DisplayName("Status")]
        [Category("Misc")]
        [Description("")]
        public float Status
        {
            get { return GetCaryParameterValue(InstrumentParameterID.RESP_STATUS); }
        }

        [Description("")]
        [Category("Misc")]
        [DisplayName("Target Signal Noise")]
        public float TargetSignalNoise
        {
            get { return GetCaryParameterValue(InstrumentParameterID.RESP_TARGET_SIGNAL_NOISE); }
            set { TryCarySetParameterValue(InstrumentParameterID.RESP_TARGET_SIGNAL_NOISE, value); }
        }

        [Category("Misc")]
        [Description("")]
        [DisplayName("Timer")]
        public float Timer
        {
            get { return GetCaryParameterValue(InstrumentParameterID.RESP_TIMER); }
            set { TryCarySetParameterValue(InstrumentParameterID.RESP_TIMER, value); }
        }

        [DisplayName("Averaging")]
        [Category("UV Visible")]
        [Description("This is the unadjusted averaging read time in chopper cycles.")]
        public int UvVisAveraging
        {
            get { return GetCaryParameterValueAsInt(InstrumentParameterID.RESP_UVVIS_AVERAGING); }
            set { SetCaryParameter(InstrumentParameterID.RESP_UVVIS_AVERAGING, value); }
        }

        [DisplayName("Interval")]
        [Category("UV Visible")]
        [Description("UV Visible interval")]
        public float UvVisInterval
        {
            get { return GetCaryParameterValue(InstrumentParameterID.RESP_UVVIS_INTERVAL); }
            set { TryCarySetParameterValue(InstrumentParameterID.RESP_UVVIS_INTERVAL, value); }
        }

        [Description("This is the slit width (nm) for a UV/Vis scan. The default value is 1.")]
        [DisplayName("Slit Width")]
        [Category("UV Visible")]
        public float UvVisSlitWidth
        {
            get { return GetCaryParameterValue(InstrumentParameterID.RESP_UVVIS_SLIT_WIDTH); }
            set { TryCarySetParameterValue(InstrumentParameterID.RESP_UVVIS_SLIT_WIDTH, value); }
        }

        [DisplayName("Wave Number Interval")]
        [Category("UV Visible")]
        [Description("")]
        public float UvVisWaveNumberInterval
        {
            get { return GetCaryParameterValue(InstrumentParameterID.RESP_UVVIS_WAVENUMBER_INTERVAL); }
            set { TryCarySetParameterValue(InstrumentParameterID.RESP_UVVIS_WAVENUMBER_INTERVAL, value); }
        }

        [Browsable(false)]
        [Description("This is the hardware version for Cary 50/60.")]
        [Category("Misc")]
        public float VersionMain
        {
            get { return GetCaryParameterValue(InstrumentParameterID.RESP_VERSION_MAIN); }
        }

        [Category("Scan")]
        [Description(
            "The wavelength (nm)  that the instrument will drive to when the Setup() method is invoked. The default value is 500")]
        [DisplayName("Wavelength")]
        public float Wavelength
        {
            get { return GetCaryParameterValueAsInt(InstrumentParameterID.RESP_GOTO_WAVELENGTH); }
            set { TryCarySetParameterValue(InstrumentParameterID.RESP_GOTO_WAVELENGTH, value); }
        }

        [ReadOnly(true)] public int WindowHandle { get; set; } = -1;

        #endregion

        #region Interface Implementations

        [HandleProcessCorruptedStateExceptions]
        [SecurityCritical]
        public virtual void Dispose()
        {
            if (IsConnected)
                Disconnect();
            Scary.CommsSetAbortFlag(WindowHandle, true);
            Scary.CommsSetTerminated(WindowHandle);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region All other members

        public event CaryInfoEventHandler CaryInfoReceived;

        public event EventHandler Connected;

        public event EventHandler Connecting;

        public event DataEventHandler DataReceived;

        public event EventHandler Disconnected;

        public event EventHandler Disconnecting;

        public event ErrorEventHandler Error;

        public event BoolValueEventHandler OnlineChanged;

        public event CaryMessageEventHandler ParamWL;

        public event EventHandler Resetting;

        public event StatusEventHandler StatusChanged;

        public event ValueInEventHandler ValueInReceived;

        [HandleProcessCorruptedStateExceptions]
        [SecurityCritical]
        public float CaryReading(float wavelength)
        {
            return Scary.CaryReading(WindowHandle, wavelength);
        }

        public bool CaryTest(InstrumentParameterID id, int value)
        {
            return GetCaryParameterValueAsInt(id) == value;
        }

        private void CheckConnection()
        {
            if (!IsConnected)
                throw new Exception("Not connected to instrument");
        }

        public virtual bool Connect()
        {
            Connect(ConnectionTimeout, SimulatorMode);
            return IsConnected;
        }

        [HandleProcessCorruptedStateExceptions]
        [SecurityCritical]
        public virtual void Connect(int timeOut, bool simulatorMode)
        {
            if (Connecting != null) //  if (this.eventHandler_1 != null)
                Connecting(this, new EventArgs()); //  this.eventHandler_1((object) this, new EventArgs());
            Scary.SetCommsDebug(false);
            IsOnline = false;
            Debug.WriteLine($"Window Handle: {WindowHandle:X}");
            IsConnected = Scary.AttachComms(WindowHandle, timeOut, simulatorMode);
            if (!IsConnected)
                throw new Exception("Connect() failed.");
            Cary32.SetProcessMessagesProc(_cary32MessageHandler0);
            ScaryAddResponseDevice(DeviceAddress.Instrument, 8192U, true, UserMessage.UM_CARY_OFFLINE);
            ScaryAddResponseDevice(DeviceAddress.Instrument, 8194U, false, UserMessage.UM_CARY_ONLINE);
            ScaryLog(DeviceAddress.Instrument, UserMessage.UM_CARY_DATA_IN, UserMessage.UM_CARY_ERROR_IN, UserMessage.UM_CARY_STATUS_IN, UserMessage.UM_CARY_VALUE_IN);
            ScaryLog(DeviceAddress.Accessory, UserMessage.UM_ACCY_DATA_IN, UserMessage.UM_ACCY_ERROR_IN, UserMessage.UM_ACCY_STATUS_IN, 0);
            ScaryLog(DeviceAddress.SamplePreparation, UserMessage.UM_SPS_DATA_IN, UserMessage.UM_SPS_ERROR_IN, UserMessage.UM_SPS_STATUS_IN, 0);
            GetModel();
            if (Connected == null) // if (this.eventHandler_2 == null)
                return;
            Connected(this, new EventArgs()); // this.eventHandler_2((object) this, new EventArgs());
        }

        [HandleProcessCorruptedStateExceptions]
        [SecurityCritical]
        public virtual void Disconnect()
        {
            if (Disconnecting != null) // if (this.eventHandler_3 != null)
                Disconnecting(this, new EventArgs()); // this.eventHandler_3((object)this, new EventArgs());
            Cary32.SetProcessMessagesProc(null);
            Scary.CommsSetAbortFlag(WindowHandle, false);
            Scary.CaryStop(WindowHandle);
            RemoveDeviceResponse(DeviceAddress.Instrument, 8192U);
            RemoveDeviceResponse(DeviceAddress.Instrument, 8194U);
            ScaryUnlog(DeviceAddress.Instrument);
            ScaryUnlog(DeviceAddress.Accessory);
            ScaryUnlog(DeviceAddress.SamplePreparation);
            Scary.DetachComms(WindowHandle);
            IsOnline = false;
            IsConnected = false;
            if (Disconnected == null) // if (this.eventHandler_4 == null)
                return;
            Disconnected(this, new EventArgs()); // this.eventHandler_4((object)this, new EventArgs());
        }

        // ReSharper disable once MemberCanBeMadeStatic.Local
        private void DoApplicationEvents()
        {
            Application.DoEvents();
        }

        [HandleProcessCorruptedStateExceptions]
        [SecurityCritical]
        internal float GetCaryParameterValue(InstrumentParameterID instrumentParameterID0)
        {
            return Scary.CaryGet((int) instrumentParameterID0);
        }

        internal int GetCaryParameterValueAsInt(InstrumentParameterID parameterID)
        {
            return (int) GetCaryParameterValue(parameterID);
        }

        [HandleProcessCorruptedStateExceptions]
        [SecurityCritical]
        public void GetDefaultSetup()
        {
            Scary.CaryGetDefaultSetup();
        }

        public string GetErrorMessage(int errorCode)
        {
            return ErrorCode.ToString(errorCode);
        }

        public void GetModel()
        {
            SubSetup(SubSetupCode.SUB_PROM_MODEL);
            Thread.Sleep(100);
        }

        public void GetStatus()
        {
            SubSetup(SubSetupCode.SUB_STATUS);
            Thread.Sleep(100);
        }

        public void GotoWavelength(float wavelength)
        {
            CheckConnection();
            Stop();
            TryCarySetParameterValue(InstrumentParameterID.RESP_GOTO_WAVELENGTH, wavelength);
            Setup();
        }

        internal bool IsParameterValueZero(InstrumentParameterID parameterID)
        {
            return GetCaryParameterValueAsInt(parameterID) != 0;
        }

        // ReSharper disable once UnusedMember.Local
        private void method_0(CaryMessageEventArgs caryMessageEventArgs0)
        {
            // ReSharper disable once UnusedVariable
            var lparam1 = caryMessageEventArgs0.LParam;
            // ReSharper disable once UnusedVariable
            var lparam2 = caryMessageEventArgs0.LParam;
        }

        [HandleProcessCorruptedStateExceptions]
        [SecurityCritical]
        private void PassMessageToFCary(Message msg)
        {
            Debug.WriteLine($"Passing Message: {(UserMessage)msg.Msg} LParam: {msg.LParam}");
            switch ((UserMessage) msg.Msg)
            {
                case UserMessage.WM_FCARY_BASE:
                case UserMessage.UM_FCARY_STATUS:
                case UserMessage.UM_FCARY_INFO:
                case UserMessage.UM_FCARY_PARAM_WL:
                case UserMessage.UM_FCARY_TEST:
                case UserMessage.UM_FCARY_TEST_STATE:
                    Scary.FCaryMessageSink(msg.Msg, (int) msg.LParam);
                    break;
            }
        }

        // ReSharper disable once UnusedMember.Local
        // ReSharper disable once UnusedParameter.Local
        private float method_12(InstrumentParameterID instrumentParameterID0, float float0)
        {
            return GetCaryParameterValue(instrumentParameterID0);
        }

        // ReSharper disable once UnusedMember.Local
        private Enum method_13(InstrumentParameterID instrumentParameterID0, Type type0)
        {
            var num = GetCaryParameterValueAsInt(instrumentParameterID0);
            return (Enum) Enum.Parse(type0, num.ToString());
        }

        // ReSharper disable once UnusedMember.Global
        internal bool Method_2(Message message0)
        {
            var flag = false;
            switch ((UserMessage) message0.Msg)
            {
                case UserMessage.WM_FCARY_BASE:
                case UserMessage.UM_FCARY_STATUS:
                case UserMessage.UM_FCARY_INFO:
                case UserMessage.UM_FCARY_PARAM_WL:
                case UserMessage.UM_FCARY_TEST:
                case UserMessage.UM_FCARY_TEST_STATE:
                    flag = true;
                    break;
            }

            return flag;
        }

        [HandleProcessCorruptedStateExceptions]
        [SecurityCritical]
        protected virtual void RaisePropertyChanged(PropertyChangedEventArgs propertyChangedEventArgs0)
        {
            var changedEventHandler0 = PropertyChanged; 
            changedEventHandler0?.Invoke(this, propertyChangedEventArgs0);
        }

        [HandleProcessCorruptedStateExceptions]
        [SecurityCritical]
        protected void RaisePropertyChanged(string propertyName)
        {
            RaisePropertyChanged(new PropertyChangedEventArgs(propertyName));
        }

        [HandleProcessCorruptedStateExceptions]
        [SecurityCritical]
        public void Read(out float wavelength, out float frontBeam, out float rearBeam)
        {
            CheckConnection();
            if (!Scary.CaryRead(out wavelength, out frontBeam, out rearBeam))
                throw new Exception("CaryRead()");
        }

        [HandleProcessCorruptedStateExceptions]
        [SecurityCritical]
        public float Read(float wavelength)
        {
            CheckConnection();
            return Scary.CaryReading(WindowHandle, wavelength);
        }

        [HandleProcessCorruptedStateExceptions]
        [SecurityCritical]
        public void RemoveDeviceResponse(DeviceAddress device, uint status)
        {
            Scary.RemoveResponseDevice(WindowHandle, (int) device, status);
        }

        [HandleProcessCorruptedStateExceptions]
        [SecurityCritical]
        internal void ScaryAddResponseDevice(DeviceAddress deviceAddress0, uint uint0, bool bool3,
            UserMessage userMessage0)
        {
            if (!Scary.AddResponseDevice(WindowHandle, (int) deviceAddress0, uint0, bool3, (int) userMessage0))
                throw new Exception(string.Format("AddDeviceResponse() error for device = {0}", deviceAddress0));
        }

        [HandleProcessCorruptedStateExceptions]
        [SecurityCritical]
        internal void ScaryLog(DeviceAddress deviceAddress0, UserMessage userMessage0, UserMessage userMessage1,
            UserMessage userMessage2, UserMessage userMessage3)
        {
            //try/catch added KSM 2018-08-07
            try
            {
                Scary.Log(WindowHandle, (byte) deviceAddress0, (int) userMessage0, (int) userMessage1,
                    (int) userMessage2, (int) userMessage3);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        [HandleProcessCorruptedStateExceptions]
        [SecurityCritical]
        internal void ScaryUnlog(DeviceAddress deviceAddress0)
        {
            try
            {
                Scary.UnLog(WindowHandle, (byte) deviceAddress0);
            }
            catch (Exception ex)
            {
                //added KSM 2018-08-07
                Console.WriteLine(ex);
            }
        }

        [HandleProcessCorruptedStateExceptions]
        [SecurityCritical]
        internal bool SetCaryParameter(InstrumentParameterID parameterID, float value)
        {
            CheckConnection();
            return Scary.CarySet(WindowHandle, (int) parameterID, value);
        }

        [HandleProcessCorruptedStateExceptions]
        [SecurityCritical]
        public void SetCommsDebug(bool onOff)
        {
            Scary.SetCommsDebug(onOff);
        }

        [HandleProcessCorruptedStateExceptions]
        [SecurityCritical]
        public void SetSimFile(string fileName)
        {
            if (!new FileInfo(fileName).Exists)
                throw new FileNotFoundException(string.Format("The file '{0}' could not be found.", fileName),
                    fileName);
            Scary.CarySetSimFile(fileName);
        }

        [HandleProcessCorruptedStateExceptions]
        [SecurityCritical]
        public void Setup()
        {
            CheckConnection();
            if (!Scary.CarySetup(WindowHandle))
                throw new Exception("CarySetup()");
        }

        [HandleProcessCorruptedStateExceptions]
        [SecurityCritical]
        public void Start()
        {
            if (!Scary.CaryStart(WindowHandle))
                throw new Exception("CaryStart()");
        }

        [HandleProcessCorruptedStateExceptions]
        [SecurityCritical]
        public void Stop()
        {
            if (!Scary.CaryStop(WindowHandle))
                throw new Exception("CaryStop()");
        }

        [HandleProcessCorruptedStateExceptions]
        [SecurityCritical]
        public void SubSetup(SubSetupCode code)
        {
            if (!Scary.CarySubSetup(WindowHandle, (int) code))
                throw new Exception(string.Format("SubSetup Exception, code = {0}", code.ToString()));
        }

        // ReSharper disable once UnusedMethodReturnValue.Local
        private bool TryCarySetParameterValue(InstrumentParameterID instrumentParameterID0, float float0)
        {
            var flag = true;
            var num = GetCaryParameterValue(instrumentParameterID0);
            // ReSharper disable once CompareOfFloatsByEqualityOperator
            if (float0 != (double) num &&
                (flag = Scary.CarySet(WindowHandle, (int) instrumentParameterID0, float0)))
                RaisePropertyChanged(instrumentParameterID0.ToString());
            return flag;
        }

        [HandleProcessCorruptedStateExceptions]
        [SecurityCritical]
        internal virtual void ProcessMessage(Message msg)
        {
           // Debug.WriteLine($"Pre result:  {msg.Result}");
            PassMessageToFCary(msg);
           // Debug.WriteLine($"Post result: {msg.Result}");

            // Forward message and results to
            // ReSharper disable once SwitchStatementMissingSomeCases
            switch ((UserMessage) msg.Msg)
            {
                case UserMessage.UM_FCARY_INFO:
                    CaryInfoReceived?.Invoke(this, new CaryInfoEventArgs(msg));
                    break;
                case UserMessage.UM_FCARY_PARAM_WL:
                    ParamWL?.Invoke(this, new CaryMessageEventArgs(msg));
                    break;
                case UserMessage.UM_CARY_DATA_IN:
                    float float0;
                    float float1;
                    float float2;
                    if (!Scary.CaryRead(out float0, out float1, out float2) || DataReceived == null)
                        break;
                    DataReceived(this, new DataEventArgs(float0, float1, float2));
                    break;
                case UserMessage.UM_CARY_STATUS_IN:
                    StatusChanged?.Invoke(this, new StatusEventArgs(msg));
                    break;
                case UserMessage.UM_CARY_ERROR_IN:
                    Error?.Invoke(this, new ErrorEventArgs(msg));
                    break;
                case UserMessage.UM_CARY_OFFLINE:
                    IsOnline = false;
                    OnlineChanged?.Invoke(this, new BoolValueEventArgs(IsOnline));
                    break;
                case UserMessage.UM_CARY_RESET:
                    Resetting?.Invoke(this, new EventArgs());
                    break;
                case UserMessage.UM_CARY_VALUE_IN:
                    ValueInReceived?.Invoke(this, new ValueInEventArgs(msg));
                    break;
                case UserMessage.UM_CARY_ONLINE:
                    IsOnline = true;
                    OnlineChanged?.Invoke(this, new BoolValueEventArgs(IsOnline));
                    break;
            }
        }

        [HandleProcessCorruptedStateExceptions]
        [SecurityCritical]
        public void WaitNotBusy()
        {
            Scary.CaryWaitNotBusy();
        }

        [HandleProcessCorruptedStateExceptions]
        [SecurityCritical]
        public void WaitNotScanning()
        {
            Scary.CaryWaitNotScanning();
        }

        #endregion
    }
}