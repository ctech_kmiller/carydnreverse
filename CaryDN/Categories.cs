﻿// Decompiled with JetBrains decompiler
// Type: CaryDN.Categories
// Assembly: CaryDN, Version=1.0.6.0, Culture=neutral, PublicKeyToken=null
// MVID: D1AE64FC-DE05-4F0D-A2F5-6382B4200B36
// Assembly location: C:\src\Decompile\Agilent\CaryDN\1.0.6.0\Decomp\CaryDN.dll

using System.Runtime.InteropServices;



namespace CaryDN
{
    [ComVisible(false)]
    public class Categories
    {
        #region Static Fields and Constants

        // ReSharper disable InconsistentNaming
        public const string ACCESSORY = "Accessory";
        public const string DEBUG = "Debug";
        public const string DETECTOR = "Detector";
        public const string FILTER = "Filter";
        public const string GRATING = "Grating";
        public const string INTERVAL = "Interval";
        public const string LAMPS = "Lamps";
        public const string MISC = "Misc";
        public const string NIR = "Near Infra Red";
        public const string PGA = "PGA";
        public const string REGISTRATION = "Registration";
        public const string SCAN = "Scan";
        public const string SHUTTER = "Shutter";
        public const string SIMULATOR = "Simulator";
        public const string SLIT = "Slit";
        public const string STATUS = "Status";
        public const string UVVIS = "UV Visible";
        // ReSharper restore InconsistentNaming

        #endregion
    }
}