﻿// Decompiled with JetBrains decompiler
// Type: CaryDN.FCaryInfo
// Assembly: CaryDN, Version=1.0.6.0, Culture=neutral, PublicKeyToken=null
// MVID: D1AE64FC-DE05-4F0D-A2F5-6382B4200B36
// Assembly location: C:\src\Decompile\Agilent\CaryDN\1.0.6.0\Decomp\CaryDN.dll

namespace CaryDN
{
    public enum FCaryInfo
    {
        // ReSharper disable UnusedMember.Global
        // ReSharper disable InconsistentNaming
        FCARY_INFO_STOP_ACK = 1,
        FCARY_INFO_COLLECT_END = 2,
        FCARY_INFO_0NM_FUDGED = 3,
        FCARY_INFO_REMOTE_READ_PUSH = 4,
        FCARY_INFO_ACCY_AT_POS = 5,
        FCARY_INFO_ANALOG_CAL_FAILED = 6,
        FCARY_INFO_DIAG_SW_NOT_ZERO = 7,
        FCARY_INFO_FLASH_COVER_OPEN = 8,
        FCARY_INFO_MOTOR_DRIVER_ERROR = 9,
        FCARY_INFO_OVERRANGE_DETECTED = 10, // 0x0000000A
        FCARY_INFO_FWUPDATE_STARTING = 60, // 0x0000003C
        FCARY_INFO_FWUPDATE_CONNECTING = 61, // 0x0000003D
        FCARY_INFO_FWUPDATE_BOOTBLOCK = 62, // 0x0000003E
        FCARY_INFO_FWUPDATE_RESETTING = 63, // 0x0000003F
        FCARY_INFO_FWUPDATE_PROGRAMMING = 64, // 0x00000040
        FCARY_INFO_FWUPDATE_VERIFYING = 65, // 0x00000041
        FCARY_INFO_FWUPDATE_FINISHED = 66 // 0x00000042

        // ReSharper restore InconsistentNaming
        // ReSharper restore UnusedMember.Global
    }
}