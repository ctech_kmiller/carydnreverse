﻿// Decompiled with JetBrains decompiler
// Type: CaryDN.CaryMessageEventArgs
// Assembly: CaryDN, Version=1.0.6.0, Culture=neutral, PublicKeyToken=null
// MVID: D1AE64FC-DE05-4F0D-A2F5-6382B4200B36
// Assembly location: C:\src\Decompile\Agilent\CaryDN\1.0.6.0\Decomp\CaryDN.dll

using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace CaryDN
{
    [ClassInterface(ClassInterfaceType.AutoDual)]
    [ComVisible(true)]
    [ProgId("CaryDN.CaryMessageEventArgs")]
    [Guid("31F0B69F-E4BC-4328-9B0C-1DFDE3330015")]
    public class CaryMessageEventArgs : EventArgs, ICaryMessageEventArgs
    {
        #region Fields

        // naming copied from Startek source code
        // ReSharper disable once InconsistentNaming
        private readonly Message m_message;

        #endregion

        #region Constructors

        public CaryMessageEventArgs(Message message)
        {
            m_message = message;
        }

        #endregion

        #region Properties, Indexers

        /// <summary>
        ///     Returns LParam as a float.
        /// </summary>
        public float FParam
        {
            get
            {
                LParamUnion u;
                u.f = 0.0f;
                u.int32 = LParam;
                return u.f;
            }
        }

        /// <summary>
        ///     The message Parameter (LParam).
        /// </summary>
        public int LParam => m_message.LParam.ToInt32();

        /// <summary>
        ///     Returns the hi word of LParam.
        /// </summary>
        public int LParamHi => (LParam >> 16) & 0xFFFF;

        /// <summary>
        ///     Returns the lo word of LParam.
        /// </summary>
        public int LParamLO => LParam & 0xFFFF;

        /// <summary>
        ///     This a windows WM_USER message.
        /// </summary>
        public UserMessage MessageID => (UserMessage) m_message.Msg;

        /// <summary>
        ///     The message Parameter (WParam).
        /// </summary>
        public int WParam => m_message.WParam.ToInt32();

        #endregion

        #region Interface Implementations

        public override string ToString()
        {
            return
                //$"Msg {(object)m_message.Msg} ({(object)(m_message.Msg - 1024)}) WParam {(object)m_message.WParam}, LParam {(object)m_message.LParam}";
                $"Msg {m_message.Msg} ({m_message.Msg - (int) UserMessage.WM_USER}) WParam {m_message.WParam}, LParam {m_message.LParam}";
        }

        #endregion

        #region All other members

        // naming copied from Startek source code
        // ReSharper disable once UnusedMember.Global
        internal Message Message()
        {
            return m_message;
        }

        #endregion

        #region Nested Types

        [StructLayout(LayoutKind.Explicit)]
        private struct LParamUnion
        {
            [FieldOffset(0)] public float f;
            [FieldOffset(0)] public int int32;
        }

        #endregion
    }
}