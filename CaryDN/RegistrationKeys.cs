﻿// Decompiled with JetBrains decompiler
// Type: RegistrationKeys
// Assembly: CaryDN, Version=1.0.6.0, Culture=neutral, PublicKeyToken=null
// MVID: D1AE64FC-DE05-4F0D-A2F5-6382B4200B36
// Assembly location: C:\src\Decompile\Agilent\CaryDN\1.0.6.0\Decomp\CaryDN.dll

using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;

[ComVisible(false)]
// ReSharper disable once CheckNamespace
internal sealed class RegistrationKeys : List<string>
{
    #region Fields

    private bool _bool0;

    // ReSharper disable once UnusedMember.Local
    private string _string0 = string.Empty;

    #endregion

    #region Constructors

    public RegistrationKeys()
    {
        BuildKeyList();
        SetBool0(false);
        method_4();
    }

    #endregion

    #region All other members

    private void BuildKeyList()
    {
        Add("A7405-43748-8CC19-CC176-67F60");
        Add("A7415-43758-8CC29-CC276-67F61");
        Add("A7425-43768-8CC39-CC376-67F62");
        Add("A7435-43778-8CC49-CC476-67F63");
        Add("A7445-43778-8CC49-CC476-67F64");
        Add("A7455-43778-8CC49-CC476-67F65");
        Add("A7465-43778-8CC49-CC476-67F66");
        Add("A7475-43778-8CC49-CC476-67F67");
        Add("A7485-43778-8CC49-CC476-67F68");
        Add("A7495-43778-8CC49-CC476-67F69");
        Add("B7405-43748-8CC19-CC176-67F60");
        Add("B7415-43758-8CC29-CC276-67F61");
        Add("B7425-43768-8CC39-CC376-67F62");
        Add("B7435-43778-8CC49-CC476-67F63");
        Add("B7445-43778-8CC49-CC476-67F64");
        Add("B7455-43778-8CC49-CC476-67F65");
        Add("B7465-43778-8CC49-CC476-67F66");
        Add("B7475-43778-8CC49-CC476-67F67");
        Add("B7485-43778-8CC49-CC476-67F68");
        Add("B7495-43778-8CC49-CC476-67F69");
    }

    public DirectoryInfo GetAssemblyDirectoryInfo()
    {
        return new FileInfo(Assembly.GetExecutingAssembly().Location).Directory;
    }

    public bool method_2()
    {
        return _bool0;
    }

    private void SetBool0(bool bool1)
    {
        _bool0 = bool1;
    }

    private void method_4()
    {
        method_5("CaryDN.key");
    }

    private void method_5(string string1)
    {
        var fileInfo = new FileInfo(Path.Combine(GetAssemblyDirectoryInfo().FullName, string1));
        if (!fileInfo.Exists)
            return;
        method_6(fileInfo);
    }

    private void method_6(FileInfo fileInfo)
    {
        StreamReader streamReader = null;
        try
        {
            streamReader = new StreamReader(fileInfo.FullName);
            SetBool0(Contains(streamReader.ReadLine()?.Trim()));
        }
        catch (Exception ex)
        {
            //added KSM 2018-08-07
            Console.WriteLine(ex);
        }
        finally
        {
            if (streamReader != null)
            {
                streamReader.Close();
                streamReader.Dispose();
            }
        }
    }

    #endregion
}