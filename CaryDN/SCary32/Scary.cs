﻿// Decompiled with JetBrains decompiler
// Type: CaryDN.SCary32.Scary
// Assembly: CaryDN, Version=1.0.6.0, Culture=neutral, PublicKeyToken=null
// MVID: D1AE64FC-DE05-4F0D-A2F5-6382B4200B36
// Assembly location: C:\src\Decompile\Agilent\CaryDN\1.0.6.0\Decomp\CaryDN.dll

using System.Runtime.InteropServices;

// ReSharper disable InconsistentNaming

namespace CaryDN.SCary32
{
    [ComVisible(false)]
    public class Scary
    {
        #region All other members

        [DllImport("Scary.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern bool AccyBusy();

        [DllImport("Scary.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern float AccyGet(int item);

        [DllImport("Scary.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern bool AccyReSet(int hWnd, int item);

        [DllImport("Scary.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern bool AccySet(int hWnd, int item, float value);

        [DllImport("Scary.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern void AccyWaitNotBusy();

        [DllImport("Scary.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern bool AddResponseDevice(int hWnd, int device, uint status, bool transition, int message_id);

        [DllImport("Scary.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern bool AttachComms(int hWnd, int timeCount, bool simMode);

        [DllImport("Scary.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern bool CaryBusy();

        [DllImport("Scary.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern float CaryGet(int item);

        [DllImport("Scary.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern void CaryGetDefaultSetup();

        [DllImport("Scary.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern float CaryGetSerialNumber();

        [DllImport("Scary.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern bool CaryRead(out float float_0, out float float_1, out float float_2);

        [DllImport("Scary.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern float CaryReading(int hWnd, float wavelength);

        [DllImport("Scary.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern bool CaryScanning();

        [DllImport("Scary.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern bool CarySet(int hWnd, int item, float value);

        [DllImport("Scary.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern void CarySetSimFile([MarshalAs(UnmanagedType.LPStr)] string simFile);

        [DllImport("Scary.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern bool CarySetup(int hWnd);

        [DllImport("Scary.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern bool CaryStart(int hWnd);

        [DllImport("Scary.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern bool CaryStop(int hWnd);

        [DllImport("Scary.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern bool CarySubSetup(int hWnd, int item);

        [DllImport("Scary.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern void CaryWaitNotBusy();

        [DllImport("Scary.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern void CaryWaitNotScanning();

        [DllImport("Scary.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern void CommsCheckAbortFlag(int hWnd);

        [DllImport("Scary.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern void CommsSetAbortFlag(int hWnd, bool state);

        [DllImport("Scary.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern void CommsSetTerminated(int hWnd);

        [DllImport("Scary.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern bool ConditionAcc(int hWnd, int device, int value, int state);

        [DllImport("Scary.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern bool DetachComms(int hWnd);

        [DllImport("Scary.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern bool DriveAcc(int hWnd, int device, int condition, int device2, int value, int state);

        [DllImport("Scary.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern bool FCaryEnableDrumAcc(bool value);

        [DllImport("Scary.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern bool FCaryEnableXSlideAcc(bool value);

        [DllImport("Scary.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern bool FCaryGetLampFlashes(out ulong dwLo, out ulong dwHi);

        [DllImport("Scary.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern bool FCaryMessageSink(int id, int data);

        [DllImport("Scary.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern bool FCaryRecalcWLGains();

        [DllImport("Scary.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern bool FCarySetDrum(int iDrum);

        [DllImport("Scary.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern bool FCarySetLampFlashes(ulong dwLo, ulong dwHi);

        [DllImport("Scary.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern bool FCarySetXSlide(int iXSlide);

        [DllImport("Scary.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern bool FRecalcGainZeros();

        [DllImport("Scary.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern bool GoBusyAcc(int hWnd, int device, int value, int state);

        [DllImport("Scary.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern bool GotoCellAcc(int hWnd, int cellNumber);

        [DllImport("Scary.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern bool LimitAcc(int hWnd, int device, int lowValue, int highValue);

        [DllImport("Scary.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern void Log(int hWnd, byte device, int data_id, int error_id, int status_id, int value_id);

        [DllImport("Scary.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern int MeasureAcc(int hWnd, int device);

        [DllImport("Scary.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern bool MonitorAcc(int hWnd, int dev1, int dev2, int dev3, int dev4, int timeval);

        [DllImport("Scary.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern bool RampAAcc(int hWnd, int device, int rate);

        [DllImport("Scary.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern bool RampAcc(int hWnd, int device, int rate);

        [DllImport("Scary.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern bool RemoveResponseDevice(int hWnd, int device, uint status);

        [DllImport("Scary.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern void SetCommsDebug(bool onoff);

        [DllImport("Scary.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern bool SPSAlign(int hWnd);

        [DllImport("Scary.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern bool SPSBusy();

        [DllImport("Scary.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern float SPSGet(int item);

        [DllImport("Scary.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern bool SPSMix(int hWnd, int rack, int tube, int height, int volume, int control);

        [DllImport("Scary.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern bool SPSMoveTo(int hWnd, int rack, int tube, int height, int timeval);

        [DllImport("Scary.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern bool SPSPark(int hWnd, int side);

        [DllImport("Scary.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern bool SPSRinse(int hWnd, int volume);

        [DllImport("Scary.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern bool SPSSet(int hWnd, int item, float value);

        [DllImport("Scary.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern bool SPSStatus(int hWnd);

        [DllImport("Scary.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern bool SPSStir(int hWnd, int timeval);

        [DllImport("Scary.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern bool SPSStop(int hWnd);

        [DllImport("Scary.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern void SPSWaitNotBusy();

        [DllImport("Scary.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern bool SPSWiggle(int hWnd, int timeval);

        [DllImport("Scary.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern bool StopAcc(int hWnd);

        [DllImport("Scary.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.Cdecl)]
        public static extern void UnLog(int hWnd, byte device);

        #endregion

        //[DllImport("Scary.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        //public static extern bool AccyRampA(int hWnd, int device, int rate);

        //[DllImport("Scary.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        //public static extern bool AVASRASet(int hWnd, int sampleAngle, int detectorAngle);

        //[DllImport("Scary.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        //public static extern int GetLibraryHandle();

        //[DllImport("Scary.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        //public static extern bool LogicalAttachComms(int hWnd);

        //[DllImport("Scary.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        //public static extern bool LogicalDetachComms(int hWnd);

        //[DllImport("Scary.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        //public static extern bool PolarizerSet(int hWnd, float polarizerAngle);

        //[DllImport("Scary.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        //public static extern int TestGetInt(int number);

        //[DllImport("Scary.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        //public static extern int TestGetString(IntPtr buffer, int bufferLength);
    }
}