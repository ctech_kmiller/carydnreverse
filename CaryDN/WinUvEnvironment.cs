﻿// Decompiled with JetBrains decompiler
// Type: CaryDN.WinUvEnvironment
// Assembly: CaryDN, Version=1.0.6.0, Culture=neutral, PublicKeyToken=null
// MVID: D1AE64FC-DE05-4F0D-A2F5-6382B4200B36
// Assembly location: C:\src\Decompile\Agilent\CaryDN\1.0.6.0\Decomp\CaryDN.dll

using System;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;

namespace CaryDN
{
    [ComVisible(false)]
    public class WinUvEnvironment
    {
        #region Properties, Indexers

        public static DirectoryInfo AssemblyLocation =>
            new FileInfo(Assembly.GetExecutingAssembly().Location).Directory;

        public static string CaryWinUVFolder => AppDomain.CurrentDomain.BaseDirectory;

        #endregion

        #region All other members

        public static void CheckFileExists(string fileName)
        {
            var fileName1 = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, fileName);
            if (!new FileInfo(fileName1).Exists)
                throw new FileNotFoundException($"File not found: {fileName1}");
        }

        public static void CheckFiles()
        {
            CheckFileExists("Cary32.dll");
            CheckFileExists("FCary.dll");
            CheckFileExists("Scary.dll");
        }

        public static bool FileExists(string fileName)
        {
            return new FileInfo(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, fileName)).Exists;
        }

        #endregion
    }
}