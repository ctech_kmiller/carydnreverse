﻿// Decompiled with JetBrains decompiler
// Type: CaryDN.ErrorEventArgs
// Assembly: CaryDN, Version=1.0.6.0, Culture=neutral, PublicKeyToken=null
// MVID: D1AE64FC-DE05-4F0D-A2F5-6382B4200B36
// Assembly location: C:\src\Decompile\Agilent\CaryDN\1.0.6.0\Decomp\CaryDN.dll

using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace CaryDN
{
    [ComVisible(true)]
    [ClassInterface(ClassInterfaceType.None)]
    [Guid("31F0B69F-E4BC-4328-9B0C-1DFDE3330014")]
    [ProgId("CaryDN.ErrorEventArgs")]
    public class ErrorEventArgs : CaryMessageEventArgs, IErrorEventArgs
    {
        #region Constructors

        public ErrorEventArgs(Message message)
            : base(message)
        {
        }

        #endregion

        #region Properties, Indexers

        /// <inheritdoc />
        public int ErrorCode => LParam;

        #endregion

        #region Interface Implementations

        public string GetErrorMessage()
        {
            return CaryDN.ErrorCode.ToString(ErrorCode);
        }

        #endregion
    }
}