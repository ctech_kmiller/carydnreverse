﻿// Decompiled with JetBrains decompiler
// Type: CaryDN.CalibrationMode
// Assembly: CaryDN, Version=1.0.6.0, Culture=neutral, PublicKeyToken=null
// MVID: D1AE64FC-DE05-4F0D-A2F5-6382B4200B36
// Assembly location: C:\src\Decompile\Agilent\CaryDN\1.0.6.0\Decomp\CaryDN.dll

namespace CaryDN
{
    public enum CalibrationMode
    {
        SignalProcessing,
        WavelengthUvVis,
        WavelengthNIR,
        NIRMemory1,
        NIRLinearity1,
        NIRLinearity2,
        CheckWavelengthUvVis,
        CheckWavelengthNIR,
        PMZero1,
        PMZero2,
        NIRMemory2
    }
}