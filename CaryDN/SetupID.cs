﻿// Decompiled with JetBrains decompiler
// Type: CaryDN.SetupID
// Assembly: CaryDN, Version=1.0.6.0, Culture=neutral, PublicKeyToken=null
// MVID: D1AE64FC-DE05-4F0D-A2F5-6382B4200B36
// Assembly location: C:\src\Decompile\Agilent\CaryDN\1.0.6.0\Decomp\CaryDN.dll

// ReSharper disable UnusedMember.Global
// ReSharper disable InconsistentNaming

namespace CaryDN
{
    public enum SetupID
    {
        ID_BEAM_MODE,
        ID_PMV_GAIN_INDEX,
        ID_UVVIS_REF,
        ID_NIR_REF,
        ID_UVVIS_SLIT,
        ID_NIR_SLIT,
        ID_UVVIS_INTERVAL,
        ID_NIR_INTERVAL,
        ID_UVVIS_AV_CYCLES,
        ID_NIR_AV_CYCLES,
        ID_SLIT_HEIGHT,
        ID_PGA_GAIN_INDICES,
        ID_PGA_MODE,
        ID_GRATING_LAMBDA,
        ID_GRATING_MODE,
        ID_GOTO_LAMBDA,
        ID_SOURCE_LAMBDA,
        ID_GOTO_SOURCE,
        ID_SOURCE_ONOFF,
        ID_DETECTOR_LAMBDA,
        ID_DETECTOR,
        ID_GOTO_FILTER,
        ID_SHUTTERS,
        ID_TARGET_SN,
        ID_UVVIS_WN_INTERVAL,
        ID_NIR_WN_INTERVAL
    }
}