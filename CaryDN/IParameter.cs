﻿// Decompiled with JetBrains decompiler
// Type: IParameter
// Assembly: CaryDN, Version=1.0.6.0, Culture=neutral, PublicKeyToken=null
// MVID: D1AE64FC-DE05-4F0D-A2F5-6382B4200B36
// Assembly location: C:\src\Decompile\Agilent\CaryDN\1.0.6.0\Decomp\CaryDN.dll

using CaryDN;
using System.Runtime.InteropServices;

[ComVisible(true)]
[Guid("31F0B69F-E4BC-4328-9B0C-1DFDE3331009")]
[InterfaceType(ComInterfaceType.InterfaceIsDual)]
// ReSharper disable once CheckNamespace
internal interface IParameter //internal interface IParameter
{
  InstrumentParameterID ParameterID { get; }

  int Value { get; }
}
