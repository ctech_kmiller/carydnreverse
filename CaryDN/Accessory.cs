﻿// Decompiled with JetBrains decompiler
// Type: CaryDN.Accessory
// Assembly: CaryDN, Version=1.0.6.0, Culture=neutral, PublicKeyToken=null
// MVID: D1AE64FC-DE05-4F0D-A2F5-6382B4200B36
// Assembly location: C:\src\Decompile\Agilent\CaryDN\1.0.6.0\Decomp\CaryDN.dll

using System;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using CaryDN.SCary32;

namespace CaryDN
{
    [ProgId("CaryDN.Accessory")]
    [ClassInterface(ClassInterfaceType.AutoDual)]
    [ComDefaultInterface(typeof(IAccessory))]
    [ComSourceInterfaces(typeof(IAccessoryEvents))]
    [Guid("31F0B69F-E4BC-4328-9B0C-1DFDE3330002")]
    [ComVisible(true)]
    public class Accessory : IDisposable, IAccessory
    {
        #region Fields

        private int _handle = -1;

        #endregion

        #region Interface Implementations

        public bool Condition(int device, int value, int state)
        {
            return Scary.ConditionAcc(_handle, device, value, state);
        }

        public bool Drive(int device, int condition, int device2, int value, int state)
        {
            return Scary.DriveAcc(_handle, device, condition, device2, value, state);
        }

        public bool EnableDrum(bool value)
        {
            return Scary.FCaryEnableDrumAcc(value);
        }

        public bool EnableXSlide(bool value)
        {
            return Scary.FCaryEnableXSlideAcc(value);
        }

        public float GetParameter(int item)
        {
            return Scary.AccyGet(item);
        }

        public bool GoBusy(int device, int value, int state)
        {
            return Scary.GoBusyAcc(_handle, device, value, state);
        }

        public bool GotoCell(int cellNumber)
        {
            return Scary.GotoCellAcc(_handle, cellNumber);
        }

        public bool Limits(int device, int lowerLimit, int upperLimit)
        {
            return Scary.LimitAcc(_handle, device, lowerLimit, upperLimit);
        }

        public int Measure(int device)
        {
            return Scary.MeasureAcc(_handle, device);
        }

        public bool Monitor(int device1, int device2, int device3, int device4, int timeVal)
        {
            return Scary.MonitorAcc(_handle, device1, device2, device3, device4, timeVal);
        }

        public bool Ramp(int device, int rate)
        {
            return Scary.RampAcc(_handle, device, rate);
        }

        public bool RampA(int device, int rate)
        {
            return Scary.RampAAcc(_handle, device, rate);
        }

        public bool Reset(int item)
        {
            return Scary.AccyReSet(_handle, item);
        }

        public bool SetParameter(int item, float value)
        {
            return Scary.AccySet(_handle, item, value);
        }

        public bool Stop()
        {
            return Scary.StopAcc(_handle);
        }

        public void WaitNotBusy()
        {
            Scary.AccyWaitNotBusy();
        }

        public void Dispose()
        {
        }

        #endregion

        #region All other members

        public event AccessoryDataEventHandler DataReceived;

        [Description(" Occurs on Accessory error.")]
        public event ErrorEventHandler Error;

        public event StatusEventHandler StatusChanged;

        // ReSharper disable once UnusedMember.Global
        internal int GetHandle()
        {
            return _handle;
        }

        internal void SetHandle(int handle)
        {
            _handle = handle;
        }

        internal virtual void ProcessMessage(Message msg)
        {
            switch (msg.Msg)
            {
                case 1727: // UM_ACCY_DATA_IN
                    DataReceived?.Invoke(this, new AccessoryDataEventArgs(msg));
                    break;
                case 1728: // UM_ACCY_STATUS_IN
                    StatusChanged?.Invoke(this, new StatusEventArgs(msg));
                    break;
                case 1729: // UM_ACCY_ERROR_IN
                    Error?.Invoke(this, new ErrorEventArgs(msg));
                    break;
            }
        }

        #endregion
    }
}