﻿// Decompiled with JetBrains decompiler
// Type: CaryDN.BoolValueEventArgs
// Assembly: CaryDN, Version=1.0.6.0, Culture=neutral, PublicKeyToken=null
// MVID: D1AE64FC-DE05-4F0D-A2F5-6382B4200B36
// Assembly location: C:\src\Decompile\Agilent\CaryDN\1.0.6.0\Decomp\CaryDN.dll

using System;
using System.Runtime.InteropServices;

namespace CaryDN
{
    [ProgId("CaryDN.BoolValueEventArgs")]
    [ComVisible(true)]
    [Guid("31F0B69F-E4BC-4328-9B0C-1DFDE3330023")]
    [ClassInterface(ClassInterfaceType.None)]
    public class BoolValueEventArgs : EventArgs, IBoolValueEventArgs
    {
        #region Constructors

        public BoolValueEventArgs(bool value)
        {
            Value = value;
        }

        #endregion

        #region Properties, Indexers

        public bool Value { get; set; }

        #endregion
    }
}