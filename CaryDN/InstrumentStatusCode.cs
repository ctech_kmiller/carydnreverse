﻿// Decompiled with JetBrains decompiler
// Type: CaryDN.InstrumentStatusCode
// Assembly: CaryDN, Version=1.0.6.0, Culture=neutral, PublicKeyToken=null
// MVID: D1AE64FC-DE05-4F0D-A2F5-6382B4200B36
// Assembly location: C:\src\Decompile\Agilent\CaryDN\1.0.6.0\Decomp\CaryDN.dll

using System.Text;

namespace CaryDN
{
    public class InstrumentStatusCode
    {
        #region All other members

        public static bool BitTest(uint statusCode, uint mask)
        {
            return ((int) statusCode & (int) mask) != 0x0;
        }

        public static string ToString(int statusCode)
        {
            return ToString((uint) statusCode);
        }

        public static string ToString(uint statusCode)
        {
            if (statusCode == 0x0U)
                return "Ready";
            var stringBuilder = new StringBuilder();
            if (BitTest(statusCode, 0x1U))
                stringBuilder.Append("Busy, ");
            if (BitTest(statusCode, 0x2U))
                stringBuilder.Append("Resetting, ");
            if (BitTest(statusCode, 0x4U))
                stringBuilder.Append("Collecting, ");
            if (BitTest(statusCode, 0x8U))
                stringBuilder.Append("Slewing, ");
            if (BitTest(statusCode, 0x10U))
                stringBuilder.Append("Driving slit, ");
            if (BitTest(statusCode, 0x20U))
                stringBuilder.Append("Calibrating PGA, ");
            if (BitTest(statusCode, 0x40U))
                stringBuilder.Append("Waiting for user, ");
            if (BitTest(statusCode, 0x100U))
                stringBuilder.Append("Lighting UV lamp, ");
            if (BitTest(statusCode, 0x200U))
                stringBuilder.Append("Lighting VIS lamp, ");
            if (BitTest(statusCode, 0x400U))
                stringBuilder.Append("Changing grating, ");
            if (BitTest(statusCode, 0x800U))
                stringBuilder.Append("Changing source, ");
            if (BitTest(statusCode, 0x1000U))
                stringBuilder.Append("Changing filter, ");
            if (BitTest(statusCode, 0x2000U))
                stringBuilder.Append("Cary offline, ");
            if (BitTest(statusCode, 0x10000U))
                stringBuilder.Append("Changing detector, ");
            if (BitTest(statusCode, 0x20000U))
                stringBuilder.Append("Init wavelength, ");
            if (BitTest(statusCode, 0x40000U))
                stringBuilder.Append("Init mono, ");
            if (BitTest(statusCode, 0x80000U))
                stringBuilder.Append("Init slit, ");
            if (BitTest(statusCode, 0x100000U))
                stringBuilder.Append("Calibrating wavelength, ");
            if (BitTest(statusCode, 0x200000U))
                stringBuilder.Append("Calibrating PM zero, ");
            if (BitTest(statusCode, 0x800000U))
                stringBuilder.Append("SNR timeout, ");
            if (BitTest(statusCode, 0x1000000U))
                stringBuilder.Append("Calibrating NIR detector, ");
            if (BitTest(statusCode, 0x2000000U))
                stringBuilder.Append("Lid open, ");
            if (BitTest(statusCode, 0x4000000U))
                stringBuilder.Append("Over heat, ");
            if (BitTest(statusCode, 0x8000000U))
                stringBuilder.Append("Waiting for chopper, ");
            if (BitTest(statusCode, 0x10000000U))
                stringBuilder.Append("Data overrange, ");
            if (BitTest(statusCode, 0x20000000U))
                stringBuilder.Append("Data underrange, ");
            if (BitTest(statusCode, 0x4000U))
                stringBuilder.Append("End of Collect, ");
            char[] chArray = {',', ' '};
            var str = stringBuilder.ToString().Trim(chArray);
            if (stringBuilder.Length == 0x0)
                str = $"Status = {(object) statusCode}";
            return str;
        }

        #endregion

        // ReSharper disable UnusedMember.Global
        // ReSharper disable InconsistentNaming
        public const uint STATUS_BUSY = 0x1;
        public const uint STATUS_RESETTING = 0x2;
        public const uint STATUS_COLLECTING = 0x4;
        public const uint STATUS_SLEWING = 0x8;
        public const uint STATUS_DRIVING_SLIT = 0x10;
        public const uint STATUS_CALIB_PGA = 0x20;
        public const uint STATUS_WAITING_FOR_USER = 0x40;
        public const uint STATUS_LIGHTING_UV_LAMP = 0x100;
        public const uint STATUS_LIGHTING_VIS_LAMP = 0x200;
        public const uint STATUS_CHANGING_GRATING = 0x400;
        public const uint STATUS_CHANGING_SOURCE = 0x800;
        public const uint STATUS_CHANGING_FILTER = 0x1000;
        public const uint STATUS_OFFLINE = 0x2000;
        public const uint STATUS_END_OF_COLLECT = 0x4000;
        public const uint STATUS_CHANGING_DET = 0x10000;
        public const uint STATUS_INIT_WAVELENGTH = 0x20000;
        public const uint STATUS_INIT_MONO = 0x40000;
        public const uint STATUS_INIT_SLIT = 0x80000;
        public const uint STATUS_CALIB_WAVELENGTH = 0x100000;
        public const uint STATUS_CALIB_PM_ZERO = 0x200000;
        public const uint STATUS_TIME_OUT = 0x400000;
        public const uint STATUS_SNR_TIMEOUT = 0x800000;
        public const uint STATUS_CALIB_NIR_DET = 0x1000000;
        public const uint STATUS_LID_OPEN = 0x2000000;
        public const uint STATUS_OVERHEAT = 0x4000000;
        public const uint STATUS_WAITING_FOR_CHOPPER = 0x8000000;
        public const uint STATUS_DATA_OVERRANGE = 0x10000000;
        public const uint STATUS_DATA_UNDERRANGE = 0x20000000;

        // ReSharper restore InconsistentNaming
        // ReSharper restore UnusedMember.Global
    }
}