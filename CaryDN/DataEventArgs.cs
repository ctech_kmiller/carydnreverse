﻿// Decompiled with JetBrains decompiler
// Type: CaryDN.DataEventArgs
// Assembly: CaryDN, Version=1.0.6.0, Culture=neutral, PublicKeyToken=null
// MVID: D1AE64FC-DE05-4F0D-A2F5-6382B4200B36
// Assembly location: C:\src\Decompile\Agilent\CaryDN\1.0.6.0\Decomp\CaryDN.dll

using System;
using System.Runtime.InteropServices;

namespace CaryDN
{
    [ComVisible(true)]
    [ProgId("CaryDN.DataEventArgs")]
    [Guid("31F0B69F-E4BC-4328-9B0C-1DFDE3330019")]
    [ClassInterface(ClassInterfaceType.AutoDual)]
    public class DataEventArgs : EventArgs
    {
        #region Constructors

        public DataEventArgs(float wavelength, float frontBeam, float rearBeam)
        {
            Wavelength = wavelength;
            FrontBeam = frontBeam;
            RearBeam = rearBeam;
        }

        #endregion

        #region Properties, Indexers

        public virtual float Abs
        {
            get
            {
                var num = 9.999;
                if (FrontBeam > 0.0)
                    num = -Math.Log10(FrontBeam);
                return (float) num;
            }
        }

        public float FrontBeam { get; set; }

        public float RearBeam { get; set; } //setter was recursive KSM 2018-08-07

        public float Wavelength { get; set; }

        #endregion

        #region All other members

        public override string ToString()
        {
            return
                $"Abs={(object) Abs:0.####}, Wavelength={(object) Wavelength}, FrontBeam={(object) FrontBeam:0.####}, RearBeam={(object) RearBeam:0.####}";
        }

        #endregion
    }
}