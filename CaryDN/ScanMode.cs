﻿// Decompiled with JetBrains decompiler
// Type: CaryDN.ScanMode
// Assembly: CaryDN, Version=1.0.6.0, Culture=neutral, PublicKeyToken=null
// MVID: D1AE64FC-DE05-4F0D-A2F5-6382B4200B36
// Assembly location: C:\src\Decompile\Agilent\CaryDN\1.0.6.0\Decomp\CaryDN.dll

namespace CaryDN
{
    public enum ScanMode
    {
        // ReSharper disable UnusedMember.Global
        Wavelength,
        Time,
        Peaking,
        WavelengthSNR,
        TimeSNR,
        Idle,
        PMVPeak,

        // ReSharper disable once InconsistentNaming
        TCalibration,

        WaveNumber,
        WaveNumberSNR
        // ReSharper restore UnusedMember.Global
    }
}