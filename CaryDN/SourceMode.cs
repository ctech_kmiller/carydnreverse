﻿// Decompiled with JetBrains decompiler
// Type: CaryDN.SourceMode
// Assembly: CaryDN, Version=1.0.6.0, Culture=neutral, PublicKeyToken=null
// MVID: D1AE64FC-DE05-4F0D-A2F5-6382B4200B36
// Assembly location: C:\src\Decompile\Agilent\CaryDN\1.0.6.0\Decomp\CaryDN.dll

// ReSharper disable UnusedMember.Global
namespace CaryDN
{
    public enum SourceMode
    {
        Auto,
        UltraViolet,
        Visible,
        Third
    }
}