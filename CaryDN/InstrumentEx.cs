﻿// Decompiled with JetBrains decompiler
// Type: CaryDN.InstrumentEx
// Assembly: CaryDN, Version=1.0.6.0, Culture=neutral, PublicKeyToken=null
// MVID: D1AE64FC-DE05-4F0D-A2F5-6382B4200B36
// Assembly location: C:\src\Decompile\Agilent\CaryDN\1.0.6.0\Decomp\CaryDN.dll

using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Runtime.ExceptionServices;
using System.Runtime.InteropServices;
using System.Security;
using System.Threading;
using System.Windows.Forms;

namespace CaryDN
{
    [ComDefaultInterface(typeof(IInstrumentEx))]
    [ComVisible(true)]
    [ProgId("CaryDN.InstrumentEx")]
    [ComSourceInterfaces(typeof(IInstrumentEvents))]
    [ClassInterface(ClassInterfaceType.AutoDual)]
    [Guid("31F0B69F-E4BC-4328-9B0C-1DFDE3330000")]
    public class InstrumentEx : InstrumentBase, IInstrumentEx
    {
        #region Fields

        private bool _initialized;
        private Form0 _hiddenForm;
        private ManualResetEvent _manualResetEvent0;
        private Thread _instrumentExThread;

        #endregion

        #region Constructors

        public InstrumentEx()
        {
            Accessory = new Accessory();
            Registration = new Registration();
        }

        #endregion

        #region Properties, Indexers

        [Category("Accessory")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public Accessory Accessory { get; }

        [Category("Registration")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public Registration Registration { get; }

        #endregion

        #region Interface Implementations

        public override bool Connect()
        {
            //CheckRegistrationKey(Registration.Key); //removed KSM2018-08-08
            if (_instrumentExThread == null)
                StartThread();
            return base.Connect();
        }

        // ReSharper disable once RedundantOverriddenMember
        public override void Disconnect()
        {
            base.Disconnect();
        }

        public override void Dispose()
        {
            base.Dispose();
            Disposing();
        }

        #endregion

        #region All other members

        public event EventHandler ThreadStart;

        /// <summary>
        ///     Check registration key to see if it is valid, and display a <see cref="MessageBox" /> if not.
        /// </summary>
        /// <param name="regKey"></param>
        private void CheckRegistrationKey(string regKey)
        {
            if (string.IsNullOrEmpty(regKey))
            {
                if (Registration.TrialDaysLeft != 0)
                    return;
                MessageBox.Show("CaryDN dll trial period has expired", "...", MessageBoxButtons.OK,
                    MessageBoxIcon.Exclamation);
            }
            else
            {
                // ReSharper disable once CollectionNeverUpdated.Local
                var registrationKeys = new RegistrationKeys();
                try
                {
                    if (!registrationKeys.Contains(regKey))
                        throw new Exception("Invalid key.");
                }
                finally
                {
                    registrationKeys.Clear();
                }
            }
        }

        private void form0_0_FormClosed(object sender, FormClosedEventArgs e)
        {
        }

        private void Disposing()
        {
            _initialized = false;
            if (_instrumentExThread == null)
                return;
            Thread.Sleep(250);
            try
            {
                _instrumentExThread.Join(1000);
                if (_instrumentExThread.IsAlive)
                    _instrumentExThread.Abort();
            }
            catch (Exception ex)
            {
                //added KSM 2018-08-07
                Console.WriteLine(ex);
                throw;
                //end of additions
            }

            _instrumentExThread = null;
        }

        [HandleProcessCorruptedStateExceptions]
        [SecurityCritical]
        private void StartThread()
        {
            using (_manualResetEvent0 = new ManualResetEvent(false))
            {
                // ReSharper disable once RedundantDelegateCreation
                _instrumentExThread = new Thread(new ThreadStart(Initialize))
                {
                    Name = nameof(InstrumentEx),
                    IsBackground = true
                };
                _instrumentExThread.SetApartmentState(ApartmentState.STA);
                _instrumentExThread.Start();
                _manualResetEvent0.WaitOne();
            }
        }

        [HandleProcessCorruptedStateExceptions]
        [SecurityCritical]
        private void Initialize()
        {
            try
            {
                _initialized = true;
                _hiddenForm = new Form0(this);
                // ReSharper disable once RedundantDelegateCreation
                _hiddenForm.FormClosed += new FormClosedEventHandler(form0_0_FormClosed);
                ThreadStart?.Invoke(null, null);
                _manualResetEvent0.Set();
                while (_initialized)
                {
                    Application.DoEvents();
                    Thread.Sleep(250);
                }

                if (_hiddenForm == null)
                    return;
                _hiddenForm.Close();
                _hiddenForm.Dispose();
                _hiddenForm = null;
            }
            catch (Exception ex)
            {
                //added KSM 2018-08-07
                Console.WriteLine(ex);
                throw;
                //end of additions                    
            }
        }

        #endregion

        #region Nested Types

        [ComVisible(false)]
        private sealed class Form0 : Form
        {
            #region Fields

            private readonly InstrumentEx _instrumentEx;

            #endregion

            #region Constructors

            public Form0(InstrumentEx instrumentEx1)
            {
                Debug.WriteLine($"Handle: {(int)Handle:X}");
                _instrumentEx = instrumentEx1;
                _instrumentEx.WindowHandle = (int)Handle;
                _instrumentEx.Accessory.SetHandle((int)Handle);
            }

            #endregion

            #region All other members

            /// <summary>
            /// The callback method called after Win32 processes a <see cref="T:System.Windows.Forms.Message" />
            /// </summary>
            /// <param name="m">The Windows <see cref="T:System.Windows.Forms.Message" /> to process. </param>
            protected override void WndProc(ref Message msg)
            {
                if (_instrumentEx != null)
                {
                    Debug.WriteLine($"Msg: {msg}");
                    _instrumentEx.ProcessMessage(msg);
                    _instrumentEx.Accessory?.ProcessMessage(msg);
                }

                base.WndProc(ref msg);
            }

            #endregion
        }

        #endregion
    }
}