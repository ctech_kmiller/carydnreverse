﻿// Decompiled with JetBrains decompiler
// Type: CaryDN.SubSetupCode
// Assembly: CaryDN, Version=1.0.6.0, Culture=neutral, PublicKeyToken=null
// MVID: D1AE64FC-DE05-4F0D-A2F5-6382B4200B36
// Assembly location: C:\src\Decompile\Agilent\CaryDN\1.0.6.0\Decomp\CaryDN.dll

// ReSharper disable InconsistentNaming
// ReSharper disable UnusedMember.Global
namespace CaryDN
{
  public enum SubSetupCode
  {
    SUB_INIT = 1,
    SUB_BEAM_MODE = 2,
    SUB_GAIN = 3,
    SUB_SLITS = 4,
    SUB_CYCLES = 5,
    SUB_SLIT_HEIGHT = 6,
    SUB_REF_LEVELS = 7,
    SUB_GRAT_WL = 8,
    SUB_DET_WL = 9,
    SUB_DETECTOR = 10, // 0x0000000A
    SUB_PGA = 11, // 0x0000000B
    SUB_SOURCE_WL = 12, // 0x0000000C
    SUB_SOURCE = 13, // 0x0000000D
    SUB_FILTER = 14, // 0x0000000E
    SUB_SNR = 15, // 0x0000000F
    SUB_WN_INT = 16, // 0x00000010
    SUB_GOTO_WL = 17, // 0x00000011
    SUB_GRATING = 18, // 0x00000012
    SUB_SOURCE_POWER = 19, // 0x00000013
    SUB_SHUTTERS = 20, // 0x00000014
    SUB_WRITE = 23, // 0x00000017
    SUB_COLLECT = 25, // 0x00000019
    SUB_STOP = 26, // 0x0000001A
    SUB_CALIBRATE = 27, // 0x0000001B
    SUB_PROM_MODEL = 28, // 0x0000001C
    SUB_STATUS = 29, // 0x0000001D
  }
}
