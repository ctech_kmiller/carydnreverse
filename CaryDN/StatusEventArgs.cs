﻿// Decompiled with JetBrains decompiler
// Type: CaryDN.StatusEventArgs
// Assembly: CaryDN, Version=1.0.6.0, Culture=neutral, PublicKeyToken=null
// MVID: D1AE64FC-DE05-4F0D-A2F5-6382B4200B36
// Assembly location: C:\src\Decompile\Agilent\CaryDN\1.0.6.0\Decomp\CaryDN.dll

using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace CaryDN
{
    [ProgId("CaryDN.StatusEventArgs")]
    [Guid("31F0B69F-E4BC-4328-9B0C-1DFDE3330013")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComVisible(true)]
    public class StatusEventArgs : CaryMessageEventArgs, IStatusEventArgs
    {
        #region Constructors

        public StatusEventArgs(Message message)
            : base(message)
        {
        }

        #endregion

        #region Properties, Indexers

        /// <summary>
        /// The Cary instrument status value.
        ///  Refer to the 'StatusCode' class for the value definitions.
        /// </summary>
        public int Value => LParam;

        #endregion
    }
}