﻿// Decompiled with JetBrains decompiler
// Type: Class1
// Assembly: CaryDN, Version=1.0.6.0, Culture=neutral, PublicKeyToken=null
// MVID: D1AE64FC-DE05-4F0D-A2F5-6382B4200B36
// Assembly location: C:\src\Decompile\Agilent\CaryDN\1.0.6.0\Decomp\CaryDN.dll

using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Globalization;
using System.Resources;

[GeneratedCode("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
[DebuggerNonUserCode]
internal sealed class Class1
{
  private static ResourceManager resourceManager_0;
  private static CultureInfo cultureInfo_0;

  internal Class1()
  {
  }

  internal static ResourceManager smethod_0()
  {
    if (object.ReferenceEquals((object) Class1.resourceManager_0, (object) null))
      Class1.resourceManager_0 = new ResourceManager("CaryDN.Resources.ResourcesEN-AU", typeof (Class1).Assembly);
    return Class1.resourceManager_0;
  }

  internal static CultureInfo smethod_1()
  {
    return Class1.cultureInfo_0;
  }

  internal static void smethod_2(CultureInfo cultureInfo_1)
  {
    Class1.cultureInfo_0 = cultureInfo_1;
  }

  internal static string smethod_3()
  {
    return Class1.smethod_0().GetString("E0001", Class1.cultureInfo_0);
  }

  internal static string smethod_4()
  {
    return Class1.smethod_0().GetString("E0064", Class1.cultureInfo_0);
  }

  internal static string smethod_5()
  {
    return Class1.smethod_0().GetString("E9301", Class1.cultureInfo_0);
  }

  internal static string smethod_6()
  {
    return Class1.smethod_0().GetString("E9302", Class1.cultureInfo_0);
  }

  internal static string smethod_7()
  {
    return Class1.smethod_0().GetString("E9303", Class1.cultureInfo_0);
  }

  internal static string smethod_8()
  {
    return Class1.smethod_0().GetString("E9304", Class1.cultureInfo_0);
  }

  internal static string smethod_9()
  {
    return Class1.smethod_0().GetString("E9305", Class1.cultureInfo_0);
  }

  internal static string smethod_10()
  {
    return Class1.smethod_0().GetString("E9306", Class1.cultureInfo_0);
  }

  internal static string smethod_11()
  {
    return Class1.smethod_0().GetString("E9307", Class1.cultureInfo_0);
  }

  internal static string smethod_12()
  {
    return Class1.smethod_0().GetString("E9308", Class1.cultureInfo_0);
  }

  internal static string smethod_13()
  {
    return Class1.smethod_0().GetString("E9309", Class1.cultureInfo_0);
  }

  internal static string smethod_14()
  {
    return Class1.smethod_0().GetString("E9310", Class1.cultureInfo_0);
  }

  internal static string smethod_15()
  {
    return Class1.smethod_0().GetString("E9311", Class1.cultureInfo_0);
  }

  internal static string smethod_16()
  {
    return Class1.smethod_0().GetString("E9312", Class1.cultureInfo_0);
  }

  internal static string smethod_17()
  {
    return Class1.smethod_0().GetString("E9313", Class1.cultureInfo_0);
  }

  internal static string smethod_18()
  {
    return Class1.smethod_0().GetString("E9314", Class1.cultureInfo_0);
  }

  internal static string smethod_19()
  {
    return Class1.smethod_0().GetString("E9315", Class1.cultureInfo_0);
  }

  internal static string smethod_20()
  {
    return Class1.smethod_0().GetString("E9316", Class1.cultureInfo_0);
  }

  internal static string smethod_21()
  {
    return Class1.smethod_0().GetString("E9317", Class1.cultureInfo_0);
  }

  internal static string smethod_22()
  {
    return Class1.smethod_0().GetString("E9318", Class1.cultureInfo_0);
  }

  internal static string smethod_23()
  {
    return Class1.smethod_0().GetString("E9319", Class1.cultureInfo_0);
  }

  internal static string smethod_24()
  {
    return Class1.smethod_0().GetString("E9320", Class1.cultureInfo_0);
  }

  internal static string smethod_25()
  {
    return Class1.smethod_0().GetString("E9321", Class1.cultureInfo_0);
  }

  internal static string smethod_26()
  {
    return Class1.smethod_0().GetString("E9322", Class1.cultureInfo_0);
  }

  internal static string smethod_27()
  {
    return Class1.smethod_0().GetString("E9323", Class1.cultureInfo_0);
  }

  internal static string smethod_28()
  {
    return Class1.smethod_0().GetString("E9324", Class1.cultureInfo_0);
  }

  internal static string smethod_29()
  {
    return Class1.smethod_0().GetString("E9325", Class1.cultureInfo_0);
  }

  internal static string smethod_30()
  {
    return Class1.smethod_0().GetString("E9326", Class1.cultureInfo_0);
  }

  internal static string smethod_31()
  {
    return Class1.smethod_0().GetString("E9327", Class1.cultureInfo_0);
  }

  internal static string smethod_32()
  {
    return Class1.smethod_0().GetString("E9328", Class1.cultureInfo_0);
  }

  internal static string smethod_33()
  {
    return Class1.smethod_0().GetString("E9329", Class1.cultureInfo_0);
  }

  internal static string smethod_34()
  {
    return Class1.smethod_0().GetString("E9330", Class1.cultureInfo_0);
  }

  internal static string smethod_35()
  {
    return Class1.smethod_0().GetString("E9332", Class1.cultureInfo_0);
  }

  internal static string smethod_36()
  {
    return Class1.smethod_0().GetString("E9340", Class1.cultureInfo_0);
  }

  internal static string smethod_37()
  {
    return Class1.smethod_0().GetString("E9341", Class1.cultureInfo_0);
  }

  internal static string smethod_38()
  {
    return Class1.smethod_0().GetString("E9610", Class1.cultureInfo_0);
  }
}
