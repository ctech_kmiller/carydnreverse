﻿// Decompiled with JetBrains decompiler
// Type: CaryDN.InstrumentEventDispId
// Assembly: CaryDN, Version=1.0.6.0, Culture=neutral, PublicKeyToken=null
// MVID: D1AE64FC-DE05-4F0D-A2F5-6382B4200B36
// Assembly location: C:\src\Decompile\Agilent\CaryDN\1.0.6.0\Decomp\CaryDN.dll

using System.Runtime.InteropServices;

namespace CaryDN
{
    [ComVisible(true)]
    public enum InstrumentEventDispId
    {
        Connected = 1,
        Connecting = 2,
        DataReceived = 3,
        Disconnected = 4,
        Disconnecting = 5,
        Error = 6,
        ValueInReceived = 7,
        Resetting = 8,
        StatusChanged = 9,
        CaryInfoReceived = 10, // 0x0000000A
        OnlineChanged = 11, // 0x0000000B
        ParamWL = 12 // 0x0000000C
    }
}