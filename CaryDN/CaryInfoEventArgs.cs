﻿// Decompiled with JetBrains decompiler
// Type: CaryDN.CaryInfoEventArgs
// Assembly: CaryDN, Version=1.0.6.0, Culture=neutral, PublicKeyToken=null
// MVID: D1AE64FC-DE05-4F0D-A2F5-6382B4200B36
// Assembly location: C:\src\Decompile\Agilent\CaryDN\1.0.6.0\Decomp\CaryDN.dll

using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace CaryDN
{
    [Guid("31F0B69F-E4BC-4328-9B0C-1DFDE3330012")]
    [ComVisible(true)]
    [ProgId("CaryDN.CaryInfoEventArgs")]
    [ClassInterface(ClassInterfaceType.None)]
    public class CaryInfoEventArgs : CaryMessageEventArgs, ICaryInfoEventArgs
    {
        #region Constructors

        public CaryInfoEventArgs(Message message)
            : base(message)
        {
        }

        #endregion

        #region Properties, Indexers

        public FCaryInfo Value => (FCaryInfo) LParam;

        #endregion
    }
}