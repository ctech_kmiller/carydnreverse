﻿// Decompiled with JetBrains decompiler
// Type: CaryDN.IInstrumentEvents
// Assembly: CaryDN, Version=1.0.6.0, Culture=neutral, PublicKeyToken=null
// MVID: D1AE64FC-DE05-4F0D-A2F5-6382B4200B36
// Assembly location: C:\src\Decompile\Agilent\CaryDN\1.0.6.0\Decomp\CaryDN.dll

using System;
using System.Runtime.InteropServices;

namespace CaryDN
{
    [Guid("31F0B69F-E4BC-4328-9B0C-2DFDE3340001")]
    [InterfaceType(ComInterfaceType.InterfaceIsIDispatch)]
    [ComVisible(true)]
    public interface IInstrumentEvents
    {
        [DispId(1)]
        void Connected(object sender, EventArgs e);

        [DispId(2)]
        void Connecting(object sender, EventArgs e);

        [DispId(3)]
        void DataReceived(object sender, DataEventArgs e);

        [DispId(4)]
        void Disconnected(object sender, EventArgs e);

        [DispId(5)]
        void Disconnecting(object sender, EventArgs e);

        [DispId(6)]
        void Error(object sender, ErrorEventArgs e);

        [DispId(7)]
        void ValueInReceived(object sender, ValueInEventArgs e);

        [DispId(8)]
        void Resetting(object sender, EventArgs e);

        [DispId(9)]
        void StatusChanged(object sender, StatusEventArgs e);

        [DispId(10)]
        void CaryInfoReceived(object sender, CaryInfoEventArgs e);

        [DispId(11)]
        void OnlineChanged(object sender, BoolValueEventArgs e);

        [DispId(12)]
        void ParamWL(object sender, CaryMessageEventArgs e);
    }
}